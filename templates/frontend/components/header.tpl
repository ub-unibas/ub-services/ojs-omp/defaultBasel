{**
 * lib/pkp/templates/frontend/components/header.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Common frontend site header.
 *
 * @uses $isFullWidth bool Should this page be displayed without sidebars? This
 *       represents a page-level override, and doesn't indicate whether or not
 *       sidebars have been configured for thesite.
 *}
{strip}
	{* Determine whether a logo or title string is being displayed *}
	{assign var="showingLogo" value=true}
	{if $displayPageHeaderLogo}
		{assign var="showingLogo" value=false}
	{/if}
{/strip}
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="de" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="de" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="de" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html lang="de" class="gt-ie8"> <![endif]-->
<html lang="{$currentLocale|replace:"_":"-"}" xml:lang="{$currentLocale|replace:"_":"-"}" class="no-js">
{if !$pageTitleTranslated}{capture assign="pageTitleTranslated"}{translate key=$pageTitle}{/capture}{/if}
{include file="frontend/components/headerHead.tpl"}

<body class="front" id="top">
<!-- class="front" muss definiert sein damit die Mobile Navigation auf der ersten "Home" Seite funktioniert; auf Unterseiten: class="col-subcol" -->
<!-- Skip links -->
<div class="skip">
	<em>Springe direkt zu:</em>
	<ul class="navSkip">
		<li><a href="#page-content" class="">Inhalt</a></li>
		<li><a href="#main-nav">Hauptmenu</a></li>
		<li><a href="#page-footer">Footer</a></li>
	</ul>
</div>
<header class="main-header">
	<!-- Metanav -->


	<nav class="container-toggle content_wrapper">
		<h6 class="hidden">Meta Navigation</h6>
		<ul class="header_nav">
			{* change language *}
			{if isset($supportedLocales) && $supportedLocales|@count}
				{foreach from=$supportedLocales item=localeName key=localeKey}
					{if $localeKey != $currentLocale}
						<li>
							<a href="{url router=$smarty.const.ROUTE_PAGE page="user" op="setLocale" path=$localeKey}">
								{$localeName}
							</a>
						</li>
					{/if}
				{/foreach}
			{/if}
			{* admin panel *}
			<li><a href="{url page="login" router=$smarty.const.ROUTE_PAGE}" title="Admin">Admin</a></li>
		</ul>
		<ul class="header_nav right">
		</ul>
	</nav>

</header>

<div class="logo-wrapper">
	<div class="main-wrapper">
		<!-- Logo -->

		<div class="logo-main">
			<!--[if gt IE 8]><!-->

			<h1 id="logo">
				<a href="https://ub.unibas.ch" title="Universität Basel"><img src="{$baseUrl}/plugins/themes/defaultBasel/media/ci/uni-basel-logo-svg.svg" alt="Universität Basel"/></a>
			</h1>

			<!--<![endif]-->
			<!--[if (lt IE 9) & (!IEMobile)]>

			<h1 id="logo">
				<a href="https://ub.unibas.ch" title="Universität Basel"><img src="{$baseUrl}/plugins/themes/defaultBasel/media/ci/uni-basel-logo-png.png" alt="Universität Basel"/></a>
			</h1>


			<![endif]-->
			<!-- Department Title -->
			<h2><a href="https://ub.unibas.ch" title="Universitätsbibliothek">Universitätsbibliothek</a></h2>
		</div>

		{* START dynamic logo insertion *}
		{capture assign="homeUrl"}
			{if $currentContext && $multipleContexts}
				{url page="index" router=$smarty.const.ROUTE_PAGE}
			{else}
				{url context="index" router=$smarty.const.ROUTE_PAGE}
			{/if}
		{/capture}
		{if $displayPageHeaderLogo && is_array($displayPageHeaderLogo)}
			<div class="logo-departement">
				<!--[if gt IE 8]><!-->
				<h1 id="logo-departement">
					<a href="{$homeUrl}" class="is_img">
						<img src="{$publicFilesDir}/{$displayPageHeaderLogo.uploadName|escape:"url"}" width="{$displayPageHeaderLogo.width|escape}" height="{$displayPageHeaderLogo.height|escape}" {if $displayPageHeaderLogo.altText != ''}alt="{$displayPageHeaderLogo.altText|escape}"{else}alt="{translate key="common.pageHeaderLogo.altText"}"{/if} />
					</a>
				</h1>
				<!--<![endif]-->
				<!--[if (lt IE 9) & (!IEMobile)]>
				<h1 id="logo-departement">
					<a href="{$homeUrl}" class="is_img">
						<img src="{$publicFilesDir}/{$displayPageHeaderLogo.uploadName|escape:"url"}" width="{$displayPageHeaderLogo.width|escape}" height="{$displayPageHeaderLogo.height|escape}" {if $displayPageHeaderLogo.altText != ''}alt="{$displayPageHeaderLogo.altText|escape}"{else}alt="{translate key="common.pageHeaderLogo.altText"}"{/if} />
					</a>
				</h1>
				<![endif]-->
			</div>
		{/if}
		{* STOP dynamic logo insertion *}

		<div class="clearfix"></div>
	</div>

	<!-- Fachbereich -->
	{if $currentJournal}
		<div class="unit-wrapper">
			<div class="main-wrapper">
				{*<h2><a href="{$baseUrl}" title="eterna">eterna</a></h2>*}
				<h2>
					<a title="Home" href="{url page="index" router=$smarty.const.ROUTE_PAGE}" class="home">
						{$currentJournal->getLocalizedName()}
					</a>
				</h2>
			</div>
		</div>
	{/if}

	<!-- Mobile -->

	<!-- Navigation Tab -->
	<ul id="navigation-tab">
		<li><a data-target="navigation" title="Menü einblenden">Menü</a></li>
	</ul>

</div>
<!-- Navbar -->


	<section id="navigation-wrapper">
		<h6 class="hidden">Navigationen</h6>

		{if $enableAnnouncements}
			<li>
				<a href="{url router=$smarty.const.ROUTE_PAGE page="announcement"}">
					{translate key="announcement.announcements"}
				</a>
			</li>
		{/if}

		<!-- Main Navigation -->
		{capture assign="primaryMenu"}
			{load_menu name="primary"}
			{*{load_menu name="primary" id="navigationPrimary" ulClass="pkp_navigation_primary"} *}
		{/capture}

		{$primaryMenu}

	</section>

		{* Wrapper for page content and sidebars *}
		{if $isFullWidth}
			{assign var=hasSidebar value=0}
		{/if}
		<div class="pkp_structure_content{if $hasSidebar} has_sidebar{/if}">
			<div id="pkp_content_main" class="pkp_structure_main" role="main">
