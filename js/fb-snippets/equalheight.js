(function($) {
	var props = {
		minHeight: 0,
		height: 'auto'
	};

	$.fn.setEqualHeight = function(o) {
		//o = $.extend({}, $.fn.setEqualHeight.defaults, o);
		var elems = this;
		var lastHighest = 0;
		var setHeight = function() {
			var highest = 0;
			for (var i = 0, len = elems.length; i < len; i++) {
				$.swap(elems[i], props, function() {
					var height = $(elems[i]).height();
					if (height > highest) {
						highest = height;
					}
				});
			}
			if (highest !== lastHighest) {
				elems
					.css('height', highest)
					.triggerHandler('equalizedHeight', {
						height: highest,
						lastHeight: lastHighest
					});
				lastHighest = highest;
			}
		};

		$(window).resize((function() {
			var timer;
			return function() {
				clearTimeout(timer);
				timer = setTimeout(setHeight, 300);
			};
		})());
		$(document).bind('emchange', setHeight);

		setHeight();
		return this;
	};

	$.fn.setEqualHeight.defaults = {};


	$.fn.setTeamListEqualHeight = function(o) {

		var listItems = $('.teamlist-item', this);
		listItems.find('[style]').removeAttr('style');
		var w = $(window).width();
		var count = 1;
		if (w < 768) {
			count = 1;
			return;
		} else if (767 < w && w < 1024) {
			count = 2;
		} else {
			count = 3;
		};


		var groups = new Array();
		var calcHeight = function(group) {
			var items = group;
			var maxHeight = 0;
			group.each(function(idx) {
				var h = $('section.additional > .text_wrapper', this).innerHeight();
				if (h > maxHeight) {
					maxHeight = h;
				}
			})
			$('section.additional > .text_wrapper', group).css({
				height: maxHeight + 50
			});
		};
		var calcUpperHeight = function(group) {
			var items = group;
			var maxHeight = 0;
			group.each(function(idx) {
				var h = $('section.teamprofile > .teamprofile-header', this).outerHeight();
				if (h > maxHeight) {
					maxHeight = h;
				}
			});
			$('section.teamprofile > .teamprofile-header', group).css({
				height: maxHeight + 20
			});
		};
		for (var i = 0; i < (listItems.length / count); i++) {
			groups[i] = $(listItems).filter(function(index) {
				// Group every 3(count) items in a group
				if (index > (i * count) - 1 && index < count * (i + 1)) {
					return this;
				}
			});
			calcHeight(groups[i]);
			calcUpperHeight(groups[i]);
		}

	};

	$.fn.setEqualHeightMediadatabase = function(o) {
		//o = $.extend({}, $.fn.setEqualHeight.defaults, o);
		var elems = this;
		var lastHighest = 0;
		var setHeight = function() {
			var highest = 0;
			for (var i = 0, len = elems.length; i < len; i++) {
				$.swap(elems[i], props, function() {
					var height = $(elems[i]).height();
					if (height > highest) {
						highest = height;
					}
				});
			}
			if (highest !== lastHighest) {
				elems
					.css('height', highest + 90)
					.triggerHandler('equalizedHeight', {
						height: highest,
						lastHeight: lastHighest
					});
				lastHighest = highest;
			}
		};

		$(window).resize((function() {
			var timer;
			return function() {
				clearTimeout(timer);
				timer = setTimeout(setHeight, 300);
			};
		})());
		$(document).bind('emchange', setHeight);

		setHeight();
		return this;
	};

	$.fn.setEqualHeightMediadatabase.defaults = {};
})(jQuery);
