/* Author: Mirjam Guderley */
(function($) {
	"use strict";
	$.widget('aperto.mainNav', {
		options: {},
		_create: function() {
			this.mobileOpener = $('#navigation-tab a[data-target="navigation"]');
			this.level1Panel = $('.navigation-lvl-1', this.element);
			this.breadcrumb = $('#breadcrumb', this.element);
			this.sublevel = $('#sublevel', this.element);
			this.mainNavHeight = this.level1Panel.outerHeight();
			this.mobilePanel = this.element;
			this.isStartpage = $('body').is('.front');
			this.checkForStartpage();
			if (this.sublevel.length !== 0) {
				this.addToggleBtn();
				this.sublevelOpen = true;
			}
			this._bindEvents();
			this.defaultMargin = 1;
		},
		_bindEvents: function() {
			var that = this;

			this.mobileOpener.on('click', function(e) {
				e.preventDefault();
				if (that.open) {
					that.hideMobile();
				} else {
					that.showMobile();
				}
			});
			$('> ul > li', this.level1Panel).on('hover', function(e) {
				e.preventDefault();

				if (Modernizr.mq('(max-width: 786px)')) {
					return;
				} else {
					var pan = $('#' + $(' > a[data-target]', this).data()['target'], that.element);
					var btn = $(this).closest('li');

					if (pan.length !== 0) {
						if (e.handleObj.origType === 'mouseleave') {
							that.menuMouseOut(btn, pan);
						} else {
							that.menuMouseOver(btn, pan);
						}
					}
				}
			});
			if (this.toggleBtn) {
				this.toggleBtn.on('click', function(e) {
					e.preventDefault();
					if (that.sublevelOpen) {
						that.hideSublevel();
					} else {
						that.showSublevel();
					}
				});
			}
			$(window).on('resize', $.Aperto.throttle(function(e) {
				that.checkForStartpage();
			}));
		},
		hideMobile: function() {
			var that = this;
			var h = this.mobilePanel.innerHeight();
			this.mobilePanel
				.animate({
					height: 0,
					opacity: 0
				}, (h / 2), function() {
					$(this).removeAttr('style').removeClass('js-open');
					that.mobileOpener.removeClass('js-open');
					that.open = false;
				});
		},
		showMobile: function() {
			var that = this;
			var h = this.mobilePanel.innerHeight();
			this.mobilePanel
				.css({
					height: 0,
					opacity: 0,
					display: 'block'
				})
				.animate({
					height: h,
					opacity: 1
				}, (h / 2), function() {
					$(this).removeAttr('style').addClass('js-open');
					that.mobileOpener.addClass('js-open');
					that.open = true;
					that.options.onPanExpand && that.options.onPanExpand.apply(that, [that, that.mobilePanel]);
				});
		},
		hideSublevel: function() {
			var that = this;
			var h = this.sublevel.innerHeight();
			this.sublevel
				.animate({
					height: 0,
					opacity: 0
				}, (h), function() {
					$(this).removeAttr('style').addClass('js-hide');
					that.toggleBtn.addClass('js-hide');
					that.sublevelOpen = false;
				});
		},
		showSublevel: function() {
			var that = this;
			var h = this.sublevel.innerHeight();
			this.sublevel
				.css({
					height: 0,
					opacity: 0,
					display: 'block'
				})
				.animate({
					height: h,
					opacity: 1
				}, (h), function() {
					$(this).removeAttr('style').removeClass('js-hide');
					that.toggleBtn.removeClass('js-hide');
					that.sublevelOpen = true;
					that.options.onPanExpand && that.options.onPanExpand.apply(that, [that, that.sublevel]);
				});
		},
		menuMouseOver: function(btn, pan) {
			var that = this;
			btn.addClass('js-active');
			pan.addClass('js-visible');
			that.sublevel.addClass('js-hide'); // Always hide on MO

			var beforeHeight = this.hasOpenContainer ? this.currentHeight : this.mainNavHeight;
			var h = pan.innerHeight() + this.mainNavHeight + this.defaultMargin;
			var speed = Math.round((600 / h)) * 100;

			this.level1Panel
				.stop()
				.css({
					height: beforeHeight
				}).animate({
					height: h
				}, speed, function() {
					that.hasOpenContainer = true;
					that.currentHeight = h;
				});
		},
		menuMouseOut: function(btn, pan) {
			var that = this;
			btn.removeClass('js-active');
			pan.removeClass('js-visible');
			if (that.sublevelOpen) {
				that.sublevel.removeClass('js-hide');
				that.sublevelOpen = true;
			} else {
				that.sublevel.addClass('js-hide');
				that.sublevelOpen = false;
			}
			var speed = Math.round((600 / this.level1Panel.outerHeight())) * 100;

			this.level1Panel
				.stop()
				.animate({
					height: this.mainNavHeight
				}, speed, function() {
					that.hasOpenContainer = false;
				});
		},
		checkForStartpage: function() {
			if (this.isStartpage) {
				this.level1Panel.removeClass('js-hide-lvl-1');
				this.level1Panel.css({
					height: ''
				});
			} else {
				if (Modernizr.mq('(max-width: 786px)')) {
					this.level1Panel.addClass('js-hide-lvl-1');
					this.level1Panel.css({
						height: ''
					});
				} else {
					this.level1Panel.removeClass('js-hide-lvl-1');
				}
			}
		},
		addToggleBtn: function() {
			this.toggleBtn = $('<li class="toggle-btn"><a href="#">' + $.i18n.getText('close') + ' / ' + $.i18n.getText('open') + '</a></li>').appendTo(this.breadcrumb.find('ul'));
		}
	});
})(window.webshims && webshims.$ || jQuery);
