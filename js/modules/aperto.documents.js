(function($) {
	"use strict";

	/*-
	 * documents module
	 * render documents via ajax
	-*/
	$.widget("aperto.documents", {
		options: {
			isDebug: false,
			loader: ".loading",
			template: "documents"
		},
		/*-
		 * query the important elements from this module and
		-*/
		_create: function() {
			this.isDebug = this.options.isDebug;
			this.isDebug && console.info("initialize ", this);
			var o = this.options;

			this.$el = $(this.element);
			this.template = Handlebars.compile(window.templates[o.template]);
			this.$loader = $(">" + o.loader, this.$el);

			this._getDocuments();
		},
		/*-
		 * get document via ajax
		-*/
		_getDocuments: function() {
			var that = this;
			this._showLoader();

			$.ajax({
				type: "GET",
				url: this.$el.attr("data-docs"),
				dataType: "json",
				success: function(data, status, xhr) {
					for (var i = 0; i < data.docs.length; i++) {
						data.docs[i].fileSize = data.docs[i].fileSize.replace('&nbsp;', ' ');
					}
					that.$el.prepend(that.template(data));
					that._hideLoader();
				},
				error: function(msg) {

				}
			})
		},
		_showLoader: function() {
			this.$loader.show();
		},
		_hideLoader: function() {
			this.$loader.hide();
		}
	});

})(window.webshims && webshims.$ || jQuery);
