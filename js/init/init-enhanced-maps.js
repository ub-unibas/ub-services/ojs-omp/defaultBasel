(function($) {
	var google;
	var isDebug = false,
		devmode = jspackager && jspackager.devmode;

	if (typeof window.aperto === "undefined") window.aperto = {};


	var projectInit = {
		immediate: function() {},
		domReadyOnce: function() {
			window.HandlerbarsTemplates();
		},
		everyDomReady: function(context) {

			initCarouselMapModule(context);

			$("#locationfinder", context).locationfinderNew();

			$("#result-list > li", context).organizationFinder();
		}
	};

	var initCarouselMapModule = function(context) {
		$('.living:not(.map)', context).carouselMapModule({
			tabSelector: ".tab-selector"
		});
		$('.living.map', context).carouselMapModule({
			monoMode: true,
			initialMode: 1,
			tabSelector: ".tab-selector",
			mobileAspectRatio: 3 / 4
		});
	};

	/* starters */
	projectInit.immediate();
	$(projectInit.domReadyOnce);
	$(function() {
		projectInit.everyDomReady(document);

		$(document).on('dommodified', function(e) {
			projectInit.everyDomReady(e.target);
		});
	});
})(window.webshims && webshims.$ || jQuery);
