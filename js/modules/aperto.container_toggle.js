/* Author: Jose Burgos */
(function($) {

	if (typeof window.aperto === "undefined") window.aperto = {};

	var delay = 200,
		timerHandleHover,
		timerTargetHover;

	/* String additional functions */
	String.prototype.hashCode = function() {
		var hash = 0,
			i, chr, len;
		if (this.length == 0) return hash;
		for (i = 0, len = this.length; i < len; i++) {
			chr = this.charCodeAt(i);
			hash = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}
		return hash;
	};
	/* Array additional functions */
	Array.prototype.containsJQuery = function(el) {
		var i = this.length - 1;

		while (i > -1) {
			if (this[i][0] === el[0]) {
				return true
			}
			i--;
		}
	}

	Array.prototype.removeJQuery = function(el) {
		var i = this.length - 1;
		var item;

		while (i > -1) {
			if (this[i][0] === el[0]) {
				return this.slice(i, 1);
			}
			i--;
		}
	}

	aperto.ContainerToggle = function(options) {
		this.defaults = {
			isDebug: false,
			animations: [],
			moderator: this,
			// animation
			animation: "slide",
			// 
			onShow: function(callback, ui, o) {
				var animation = ui.animations[o.animation];
				if (o.animation === 'slide') {
					ui.$target.slideDown(o.speed, function() {
						if (callback) {
							callback();
						}

						ui.$target.trigger("dommodified");
						ui.$target.trigger("updatedom");
						ui.$target.trigger("equalRowHeight:initialize", [{
							"target": ui.$target
						}]);

						ui.isDebug && console.log("_showAnimation complete");
						ui.$el.trigger("containerToggle:complete", [{
							"ui": ui
						}]);
						if (o.eventType === "click" && o.scrollViewportTillExpandable)
							ui._scrollViewportTillExpandable();
						// keep the auto height. jQuery weirdly sets the element's height
						// to 1px sometimes
						if (o.autoHeight)
							ui.$target.css("height", "auto");
						// hide handle when an elemenet is active if the toggleHandle attr is true
						ui._toggleHandle("hide");
					});
				} else {
					ui.$target.fadeIn(o.speed, function() {
						if (callback) {
							callback();
						}

						ui.$target.trigger("dommodified");
						ui.$target.trigger("updatedom");
						ui.$target.trigger("equalRowHeight:initialize", [{
							"target": ui.$target
						}]);

						ui.isDebug && console.log("_showAnimation complete");
						ui.$el.trigger("containerToggle:complete", [{
							"ui": ui
						}]);
						if (o.eventType === "click" && o.scrollViewportTillExpandable)
							ui._scrollViewportTillExpandable();
						// keep the auto height. jQuery weirdly sets the element's height
						// to 1px sometimes
						if (o.autoHeight)
							ui.$target.css("height", "auto");
						// hide handle when an elemenet is active if the toggleHandle attr is true
						ui._toggleHandle("hide");
					});
				}
				/*o.animations["slide"] = {
						"show": "slideDown",
						"hide": "slideUp"
					}
					// fade
				o.animations["fade"] = {
					"show": "fadeIn",
					"hide": "fadeOut"
				}*/

			},
			onHide: function(callback, ui, o) {
				var animation = ui.animations[o.animation];
				if (o.animation === 'slide') {
					ui.$target.slideUp(o.speed, function() {
						if (callback) {
							callback();
						}
						ui.isDebug && console.log("_hideAnimation complete");
						// keep the auto height. jQuery weirdly sets the element's height
						// to 1px sometimes
						if (o.autoHeight)
							ui.$target.css("height", "auto");
						// hide handle when an element is active if the toggleHandle attr is true
						ui._toggleHandle("show");
					});
				} else {
					ui.$target.fadeOut(o.speed, function() {
						if (callback) {
							callback();
						}
						ui.isDebug && console.log("_hideAnimation complete");
						// keep the auto height. jQuery weirdly sets the element's height
						// to 1px sometimes
						if (o.autoHeight)
							ui.$target.css("height", "auto");
						// hide handle when an element is active if the toggleHandle attr is true
						ui._toggleHandle("show");
					});
				}

			},
			context: document,
			handle: ".toggle",
			expandable: ".expandable",
			activeClass: "active",
			disableClass: "off",
			subnavClass: "on",
			beforeSubnavClass: "before-subnav",
			speed: 0,
			scrollTop: false,
			enableAria: false,
			focus: true,
			scrollViewportTillExpandable: false,
			desktopWith: 1024,
			mobileWidth: 768,
			noBorderClass: undefined,
			activateSmartFocus: true,
			closeOnFocusout: false,
			// set target height to auto
			autoHeight: false,
			// tab mode
			tabMode: undefined,
			initialTab: undefined,
			initialTabClass: "open",
			// toggle content
			initialContent: ".initial-content",
			toggleContent: false,
			equalHeight: false,
			// toggle handle
			toggleHandle: undefined,
			toggleVisibilityToggleHandle: true,
			// device adaptable targets: if true, the expandable content
			// with the current device class will not act as an expandable content
			// in other words, it will not be faded in or out
			deviceAdaptedTargets: false,
			// disable toggle for the selected devices (desktop || tablet || mobile)
			rejectedDevices: [],
			// events @todo
			// when the plugin is initialized
			start: function() {},
			// 
			before: function() {},
			after: function() {},
			complete: function() {},
			resize: function() {}
		};

		this.options = $.extend(this.defaults, options);

		this.$el = $(this.options.context, this.element);
		this.$widgetSelector = $(this.options.selector, this.$el);
		this.isDebug = this.options.isDebug;

		this._initWidget();
	};

	aperto.ContainerToggle.prototype = {
			_initWidget: function() {
				var that = this,
					o = this.options;

				if (this.$el.attr("data-" + o.selector.hashCode())) {
					return;
				} else {
					this.$el.attr("data-" + o.selector.hashCode(), true);
				}

				this.activeEls = []

				this.viewportWidth = window.innerWidth;
				this._getDeviceMode(o);
				this._defineAnimations(o);
				// initialize widget
				this.$widgetSelector.containerToggle(this.options);

				this._bindEvents(this, o);
				// calculates the container size in order to equalize this height
				$.webshims.ready('WINDOWLOAD', function() {
					that._equalDivsHeight(o);
					// execute after function
					o.after();
					// open initial tab, if there is one
					that._openInitialTab(o);
				});
				$(window).on('emchange', function() {
					that._equalDivsHeight(o);
				});
			},
			_bindEvents: function(that, o) {
				// first init
				this._changeMouseEvents(o);
				// calculates the container size in order to equalize this height
				$(window).smartresize(function(event) {
					// do not execute this if the width has not changed
					if (that.viewportWidth === window.innerWidth) return;
					that.viewportWidth = window.innerWidth;
					that._changeMouseEvents(o, event);
					that._deviceAdaptedTargets(o, event);
					that.sizeUpdate();
				});
			},
			_defineAnimations: function(o) {
				// define available animations
				// slide
				o.animations["slide"] = {
						"show": "slideDown",
						"hide": "slideUp"
					}
					// fade
				o.animations["fade"] = {
					"show": "fadeIn",
					"hide": "fadeOut"
				}
			},
			// decide which kind of events will toggle the elements
			_changeMouseEvents: function(o, event) {
				this._getDeviceMode(o);
				var isInitialized = this.$widgetSelector.data("aperto-containerToggle"),
					isDesktop = this.mode === "desktop",
					isRejectedDevice = o.rejectedDevices.indexOf(this.mode) > -1;

				if (!isInitialized) return;

				switch (true) {
					// disable and unbind events for rejected devides
					case isRejectedDevice:
						this.$widgetSelector.containerToggle("disableEl", undefined, true);
						this.$widgetSelector.containerToggle("unbindMouseEvents");
						break;
					case (o.enableHover && isDesktop):
						this.$widgetSelector.containerToggle("bindMouseEvents", "hover");
						break;
					default:
						this.$widgetSelector.containerToggle("bindMouseEvents", "click");
				}
			},
			_deviceAdaptedTargets: function(o, event) {
				this._getDeviceMode(o);

				if (!o.deviceAdaptedTargets) return;

				this.$widgetSelector.containerToggle("getTarget", "hover");
			},
			activeHandle: function(children) {
				this.$widgetSelector.containerToggle("showHandleWithSameTarget", children);
			},
			disableHandle: function(children) {
				this.$widgetSelector.containerToggle("hideHandleWithSameTarget", children);
			},
			disableElement: function(children) {
				var i = 0;
				var length = this.activeEls.length;
				// reset the no border tabs
				this.resetLinkBorders(this.options);

				while (this.activeEls.length) {
					this.isDebug && console.log("Array: ", this.activeEls);
					this.activeEls.pop().containerToggle("disableEl", undefined, children);
				}
			},
			sizeUpdate: function() {
				var o = this.options;

				this._equalDivsHeight(o);
				// fire resize callback function
				o.resize();
				// open initial tab, if there is one
				this._openInitialTab(o);
			},
			_openInitialTab: function(o) {
				var initialTab = typeof o.initialTab !== "undefined" ? o.initialTab : this.$widgetSelector.parent().closest("." + o.initialTabClass).index();
				var $item;

				if (typeof initialTab !== "undefined" && initialTab > -1) {
					this.$widgetSelector.eq(initialTab).containerToggle("enableEl");
					return;
				}

				if (this.$widgetSelector.hasClass(o.initialTabClass)) {
					this.$widgetSelector.each(function(index, item) {
						var $item = $(item)
						if ($item.hasClass(o.initialTabClass)) {
							$item.containerToggle("enableEl");
							return;
						}
					});
				}
			},
			// Toggle Content: height calculation and equealization
			_equalDivsHeight: function(o) {
				if (!o.equalHeight) return;

				if (o.toggleContent) this.$widgetSelector.containerToggle("disableEl");
				// set the height to auto so that the new heights can be calculated
				$(o.expandable + ", " + o.initialContent, this.$el).css("height", "auto");

				this.$widgetSelector.containerToggle("toggleVisibility", false);

				this._setMaxHeight(o);

				this.$widgetSelector.containerToggle("toggleVisibility", true);
			},
			_setMaxHeight: function(o) {
				var that = this,
					$elements = $(o.expandable + ", " + o.initialContent, this.$el),
					height = 0;

				$elements.each(function(index, item) {
					var itemHeight = $(item).height();

					if (height < itemHeight)
						height = itemHeight;
				});

				this.isDebug && console.log("Resulting Height => ", height);
				$elements.height(height);
				// height event
				this.$el.trigger({
					type: "containerToggle:height",
					elements: $elements
				});
			},
			// set mode variable to the current device
			// possible devices: desktop || table || mobile
			_getDeviceMode: function(o) {
				var viewportWidth = window.innerWidth;
				// save the previous mode
				this.previousMode = this.mode;

				switch (true) {
					case (viewportWidth <= o.mobileWidth):
						this.mode = "mobile";
						break;
					case (viewportWidth > o.mobileWidth && viewportWidth < o.desktopWith):
						this.mode = "tablet";
						break;
					default:
						this.mode = "desktop";
				}

				this.isDebug && console.log("Device: ", this.mode);
			},
			// 
			removeBorderPreviousLink: function(index, o, isSubnav) {
				// Do not execute if the removal of borders is not needed
				if (!o.noBorderClass) return;

				var noBorderClass = isSubnav ? o.noBorderClass + " before-subnav" : o.noBorderClass;

				if (index != 0)
					this.$noBorderItem = this.$widgetSelector.eq(index - 1).addClass(noBorderClass);
			},
			// 
			resetLinkBorders: function(o) {
				this.$noBorderItem && !this.$noBorderItem.hasClass(o.beforeSubnavClass) && this.$noBorderItem.removeClass(o.noBorderClass);
			}
		}
		/*-
		 * containerToggle
		 [ widget (ui) ]

		 * Generates a Tab Control UI or an accordion

		 > Options
		 - isDebug: (boolean) <false>
		 - handle: (string) <".toggle">
		 - expandable: (string) <".expandable">
		 - speed: (number) <0>
		 - scrollTop: (boolean) <false>
		 - activeClass: (string) <active>
		 - enableAria: (boolean) <false>
		 - context: (string) <document> widget context
		 - toggleHandle (string) <undefined> hide the handle when the expandable content is visible and show the handle again when expa. isn't visible
		 - keepState (string) <undefined> 

		 > Events

		 > Usage
		 | $(elm).containerToggle(options);
		 -*/
	$.widget('aperto.containerToggle', {
		options: {},
		_create: function() {
			var that = this,
				o = this.options;

			this.$el = $(this.element[0]);
			this.moderator = o.moderator;
			this.animations = o.animations;
			// get target/s
			this.getTarget();

			this.$focusable = $("a, input", this.$target);
			this.$toggleHandle = typeof o.toggleHandle !== "undefined" ?
				(this.$target.siblings(o.toggleHandle).size() > 0 ? this.$target.siblings(o.toggleHandle) : $(o.toggleHandle, this.$target)) : undefined;
			// in case there is toggle content
			this.$initialContent = this.$target.siblings(o.initialContent);
			// define animations
			// control variables
			this.isStarted = false;
			this.isActive = false;
			this.disable = false;

			var subnavHandle = o.subnavHandle;
			this.isSubnavHandle = subnavHandle && subnavHandle.apply(this).hasClass(o.subnavClass);

			this.isDebug = o.isDebug;
			// check if it is a subnavigation menu
			this._subnavigationRole(o);

			this._bindEvents();
		},
		_bindEvents: function() {
			var that = this;

			this.$el.on("containerToggle:open", this.enableEl.bind(this));

			$(document).on("containerToggle:targetBlur", this._targetBlur.bind(this));

			if (typeof this.$toggleHandle !== "undefined") {
				this.isDebug && console.log("ToggleHandle bind event");
				this.$toggleHandle.on("click", this.disableEl.bind(this));
			}

			this.$target.on("activateWidget", function(event) {
				//if(event.moderator === that.moderator)
				that.activateWidget(event)
			});
			this.$target.on("deactivateWidget", function(event) {
				//if(event.moderator === that.moderator)
				that.deactivateWidget(event)
			});

			if (this.options.activateSmartFocus)
				this.$el.on("focus", this._tabFocus.bind(this));
		},
		bindMouseEvents: function(status) {
			var that = this,
				o = this.options,
				$target = this.$target.parent();

			this.unbindMouseEvents($target);

			if (status === "click" || this.isSubnavHandle) {
				this.$el.on("click", function(event) {
					that._onClickHandle(event)
				});
				this.$el.off("mouseover");
				this.$el.off("mouseout");
				if ($target) {
					$target.off("mouseover");
					$target.off("mouseout");
				}
			} else {
				function mouseoverHandle(event) {
					if (that.isActive && !o.tabMode) return;

					timerHandleHover = setTimeout(function() {
						that._onClickHandle(event);
					}, delay);
				};

				function mouseoutHandle(event) {
					clearTimeout(timerHandleHover);
					timerTargetHover = setTimeout(function() {
						that.moderator.disableElement(this);
						that.moderator.resetLinkBorders(o);
					}, delay);
				};

				function mouseoverTarget(event) {
					clearTimeout(timerTargetHover);
				};

				function mouseoutTarget(event) {
					that.isDebug && console.log("event.target: ", event.target, ", event.currentTarget: ",
						event.currentTarget, ", event.relatedTarget: ", event.relatedTarget);
					if ($(event.currentTarget).find(event.target).size() > 0) return;

					that.moderator.disableElement(this);
					that.moderator.resetLinkBorders(o);
				};

				this.$el.off("click");
				this.$el.on("mouseover", mouseoverHandle);
				this.$el.on("mouseout", mouseoutHandle);
				$target.on("mouseover", mouseoverTarget);
				$target.on("mouseout", mouseoutTarget);
			}
		},
		unbindMouseEvents: function($target) {
			this.$el.off("click");
			this.$el.off("mouseover");
			this.$el.off("mouseout");
			if ($target) {
				$target.off("mouseover");
				$target.off("mouseout");
			}
		},
		_tabFocus: function(e) {
			var that = this;

			this.$el.on("keydown", function(event) {
				var activate = function() {
					that.enableEl(undefined, function() {
						that.$focusable = $(":focusable", that.$target).filter(":visible");
						if ($(that.$focusable).filter("[tabindex='0']").size() > 0) {
							// $("a[tabindex=0]", that.$target).first().focus();
							return;
						} else if (!event.shiftKey || event.which === 13) {
							// backward
							that.$focusable.first().focus();
						} else {
							// foreward
							that.$focusable.last().focus();
						}

						this.isDebug && console.log("Tab focus index => ", that.$focusable.index(document.activeElement), ", => ", document.activeElement);

						that._focusTargetArea();
					});
				}

				if (event.which === 9 && (that.options.enableHover || that.isActive)) {
					event.preventDefault();
					activate();
				}

				if (event.which === 13 && !that.isActive) {
					event.preventDefault();
					activate();
				}
			});
		},
		_targetBlur: function(event) {
			if (event.currentFocusedTarget[0] !== this.$target[0]) {
				this.$target.off("keydown");
			}
		},
		_focusTargetArea: function() {
			var that = this,
				index,
				i = 0,
				length = this.$focusable.size(),
				currentElIndex = this.moderator.$widgetSelector.index(this.$el);

			this.$el.off("keydown");

			$(document).trigger({
				type: "containerToggle:targetBlur",
				currentFocusedTarget: this.$target
			});
			// prevent a target with two key events at the same time
			this.$target.off("keydown");

			this.$target.on("keydown", function(event) {
				if (event.which === 9) {
					event.preventDefault();

					index = that.$focusable.index(document.activeElement);

					if (event.shiftKey) {
						// backward
						if (index - 1 < 0) {
							var $previousSelectedEl = currentElIndex - 1 > -1 ? that.moderator.$widgetSelector.get(currentElIndex - 1) : undefined;
							var previousCallback;
							if ($previousSelectedEl) {
								$previousSelectedEl.focus();
							} else {
								previousCallback = function() {
									var $previousMainFocused = $(":focusable").index(that.$el);
									if ($previousMainFocused > -1) {
										$(":focusable").eq($previousMainFocused - 1).focus();
										that.$target.off("keydown");
									}
								};
							}
							// if it is tab mode, there must be always an opened content
							that.options.closeOnFocusout && that.disableEl(undefined, undefined, previousCallback);
							$(this).off("keydown");
						} else {
							$(that.$focusable[index - 1]).focus();
						}
					} else {
						// foreward
						if (index + 1 >= length) {
							var $nextSelectedEl = that.moderator.$widgetSelector.get(currentElIndex + 1);
							var nextCallback;
							if ($nextSelectedEl) {
								$nextSelectedEl.focus();
							} else {
								nextCallback = function() {
									var $nextMainFocused = $(":focusable").index(that.$el);
									if ($nextMainFocused > -1) {
										$(":focusable").eq($nextMainFocused + 1).focus();
										that.$target.off("keydown");
									}
								};
							}
							// if it is tab mode, there must be always an opened content
							that.options.closeOnFocusout && that.disableEl(undefined, undefined, nextCallback);
							$(this).off("keydown");
						} else {
							$(that.$focusable[index + 1]).focus();
						}
					}
				}
			});
		},
		_subnavigationRole: function(o) {
			if (!this.isSubnavHandle)
				return

			this.$target = $(o.subnavEl);
			o.activeClass = o.subnavClass;
			this.moderator.removeBorderPreviousLink(this.$el.parent().index(), o, true);

			this.activateWidget();
		},
		/*-
		 * if the expandable target is inside the clickable element, the data-target won't be
		 * necessary. The closest expandable object to the handle element will be searched.
		 -*/
		getTarget: function() {
			var targetId = this.$el.data("target"),
				$target = typeof targetId !== "undefined" && targetId.length > 0 ? $("#" + targetId) : this.$el.closest(this.options.expandable),
				$target = $target.size() < 1 ? this.$el.siblings(this.options.expandable) : $target,
				$target = $target.size() < 1 ? $(this.options.expandable, this.$el) : $target;

			this.isDebug && console.log("Data target:", $target);
			this.$target = this.options.deviceAdaptedTargets ? $target.not("." + this.moderator.mode) : $target;
			// make visible the device adapted targets
			// read the comment on the line 40
			if (this.options.deviceAdaptedTargets) {
				$target.filter("." + this.moderator.mode).show();
				this.disableEl();
			}
		},
		_isStarted: function() {
			if (!this.isStarted) {
				this.isDebug && console.log("containerToggle:start", this);
				this.$el.trigger("containerToggle:start", [{
					"ui": this
				}]);
				this.isStarted = true;
			}
		},
		// END Focus functions
		_onClickHandle: function(event) {
			event.preventDefault();

			if (this.disable) return;

			this.isDebug && console.log("Click event", event);
			// add the current target to the expandable array
			if (this.isActive && !this.options.tabMode)
				this.disableEl(event);
			else
				this.enableEl(event);

			this._scrollToTop();
		},
		enableEl: function(event, callback) {
			this.isDebug && console.log("is inactive", this.$el);
			var that = this,
				o = this.options;

			this._isStarted();

			if (!this.isSubnavHandle) this.moderator.disableElement(this);

			this.moderator.activeHandle(this);
			// remove the previous item's border
			this.moderator.removeBorderPreviousLink(this.$el.parent().index(), o);

			this.activateWidget();
			this.$target.trigger({
				type: "activateWidget",
				moderator: this.moderator
			});

			try {
				if (event)
					o.eventType = event.type;
				o.onShow(callback, this, o);
			} catch (e) {
				console.log("Animation not available ", e);
			}
			// refactor that
			this.moderator.$widgetSelector.each(function(idx) {

				if ($($(this).parent()).is('.on')) {
					$('#sublevel').hide();
				}
			});
			// trigger change event
			this.$el.trigger("containerToggle:change");
		},
		disableEl: function(event, children, callback) {
			this.isDebug && console.log("disableEl ", event, ", children: ", children);

			event && event.preventDefault();

			if (!this.isActive && (typeof children !== "undefined" && this.isSubnavHandle && !children.isSubnavHandle)) {
				this.$target.show();
			} else {
				var o = this.options;
				this.deactivateWidget();
				this.$target.trigger({
					type: "deactivateWidget",
					moderator: this.moderator
				});

				try {
					o.onHide(callback, this, o);
				} catch (e) {
					console.log("Animation not available ", e);
				}

			}

		},
		_scrollViewportTillExpandable: function() {
			this.isDebug && console.log("_scrollViewportTillExpandable: ", this.$target);
			var $item = this.$target.closest('li.item').size() > 0 ? this.$target.closest('li.item') : this.$target.parent();

			$("body, html").animate({
				scrollTop: $item.offset().top
			}, 100);
		},
		_toggleHandle: function(status) {
			if (typeof this.$toggleHandle !== "undefined") {
				if (status === "hide") {
					this.options.toggleVisibilityToggleHandle && this.$el.hide();
					this.$toggleHandle.addClass(this.options.activeClass);
					this.unbindMouseEvents();
				} else {
					this.options.toggleVisibilityToggleHandle && this.$el.show();
					this.$toggleHandle.removeClass(this.options.activeClass);
					this.bindMouseEvents("click");
				}
			}
		},
		showHandleWithSameTarget: function(elem) {
			this.isDebug && console.log("containerToggle:activeHandle", event)
			if (this.$target[0] === elem.$target[0]) {
				this.$el.addClass(this.options.activeClass);
				this.$target.trigger({
					type: "activateWidget",
					moderator: this.moderator
				});
			}
		},
		hideHandleWithSameTarget: function(elem) {
			this.isDebug && console.log("containerToggle:disableHandle", event)
			if (this.$target[0] === elem.$target[0]) {
				this.$el.removeClass(this.options.activeClass);
				this.$target.trigger({
					type: "deactivateWidget",
					moderator: this.moderator
				});
			}
		},
		enableHandle: function() {
			this.$el.removeClass(this.options.disableClass);
		},
		disableHandle: function() {
			this.$el.addClass(this.options.disableClass);
		},
		_scrollToTop: function() {
			if (this.options.scrollTop) {
				$("body").scrollTop(0);
				$(document).scrollTop(0);
			}
		},
		activateWidget: function(event) {
			this.isDebug && console.log("activateWidget", event)
			this.isActive = true;
			if (!this.moderator.activeEls.containsJQuery(this.$el)) {
				this.moderator.activeEls.push(this.$el);
			}
			this.$el.addClass(this.options.activeClass);
		},
		deactivateWidget: function(event) {
			this.isDebug && console.log("deactivateWidget", event)
			this.isActive = false;
			this.moderator.activeEls.removeJQuery(this.$el);
			this.$el.removeClass(this.options.activeClass);
		},
		disableWidget: function() {
			this.disable = true;
		},
		enableWidget: function() {
			this.disable = false;
		},
		// toggle content functions
		// make it display block and visibility hidden in order
		// to measure the size and equalize the divs	
		toggleVisibility: function(visible) {
			this.$target.css({
				visibility: visible ? "visible" : "hidden",
				display: visible ? "none" : "block"
			});
		}
	});
})(window.webshims && webshims.$ || jQuery);
