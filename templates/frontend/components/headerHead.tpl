{**
 * templates/frontend/components/headerHead.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2000-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site header <head> tag and contents.
 *}
<head>
	<meta charset="{$defaultCharset|escape}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<title>
		{$pageTitleTranslated|strip_tags}
		{* Add the journal name to the end of page titles *}
		{if $requestedPage|escape|default:"index" != 'index' && $currentContext && $currentContext->getLocalizedName()}
			| {$currentContext->getLocalizedName()}
		{/if}
	</title>

	<link  rel="shortcut icon" type="image/x-icon" href="{$baseUrl}/plugins/themes/defaultBasel/img/unibas.ico" />
	<!-- Webfonts einbinden (dies darf auf keinen Fall geändert werden. Lizensierung nur für Subdomains von unibas.ch) -->
	<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/b023cf84-f602-49db-8821-883d5e34f430.css"/>
	<!--[if gt IE 8]><!-->
	<link rel="stylesheet" media="screen,projection" href="{$baseUrl}/plugins/themes/defaultBasel/css/main.css">
	<link rel="stylesheet" media="print" href="{$baseUrl}/plugins/themes/defaultBasel/css/print.css">
	<!--<![endif]-->
	{* mhi: moved to DefaultBaselChildThemePlugin.inc.php
	<script src="{$baseUrl}/plugins/themes/defaultBasel/js/__basic-behaviour.js" type="text/javascript"></script>
	<script src="{$baseUrl}/plugins/themes/defaultBasel/js/__base-modules.js" type="text/javascript"></script>
	<script src="{$baseUrl}/plugins/themes/defaultBasel/js/__base-modules2.js" type="text/javascript"></script>
	<script src="{$baseUrl}/plugins/themes/defaultBasel/js/__enhanced-modules.js" type="text/javascript"></script>
	*}
	<!--[if (lt IE 9) & (!IEMobile)]>
	<script src="{$baseUrl}/plugins/themes/defaultBasel/js/selectivizr-min.js" type="text/javascript"></script>
	<link type="text/css" rel="stylesheet" href="{$baseUrl}/plugins/themes/defaultBasel/css/old-ie.css" />
	<![endif]-->
	<!--[if gt IE 8]><!-->
	{* mhi: moved to DefaultBaselChildThemePlugin.inc.php
	<script>
		loadCSS("{$baseUrl}/plugins/themes/defaultBasel/css/main.icons.css");
	</script>
	*}
	<!--<![endif]-->

	{load_header context="frontend"}
	{load_stylesheet context="frontend"}
</head>
