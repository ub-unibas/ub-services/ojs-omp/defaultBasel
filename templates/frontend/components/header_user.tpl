{**
 * lib/pkp/templates/frontend/components/header.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Common frontend site header.
 *
 * @uses $isFullWidth bool Should this page be displayed without sidebars? This
 *       represents a page-level override, and doesn't indicate whether or not
 *       sidebars have been configured for thesite.
 *}
{strip}
	{* Determine whether a logo or title string is being displayed *}
	{assign var="showingLogo" value=true}
	{if $displayPageHeaderLogo}
		{assign var="showingLogo" value=false}
	{/if}
{/strip}
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="de" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="de" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="de" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html lang="de" class="gt-ie8"> <![endif]-->
<html lang="{$currentLocale|replace:"_":"-"}" xml:lang="{$currentLocale|replace:"_":"-"}" class="no-js">
{if !$pageTitleTranslated}{capture assign="pageTitleTranslated"}{translate key=$pageTitle}{/capture}{/if}
{include file="frontend/components/headerHead.tpl"}

<body class="pkp_page_{$requestedPage|escape|default:"index"} pkp_op_{$requestedOp|escape|default:"index"}{if $showingLogo} has_site_logo{/if}" dir="{$currentLocaleLangDir|escape|default:"ltr"}">

<!-- class="front" muss definiert sein damit die Mobile Navigation auf der ersten "Home" Seite funktioniert; auf Unterseiten: class="col-subcol" -->
<!-- Skip links -->
<div class="skip">
	<em>Springe direkt zu:</em>
	<ul class="navSkip">
		<li><a href="#page-content" class="">Inhalt</a></li>
		<li><a href="#main-nav">Hauptmenu</a></li>
		<li><a href="#page-footer">Footer</a></li>
	</ul>
</div>
<header class="main-header">
	<!-- Metanav -->


	<nav class="container-toggle content_wrapper">
		<h6 class="hidden">Meta Navigation</h6>
		<ul class="header_nav">
			{*<li><a href="#" title="Show website in English">English</a></li>*}
			<li><a href="{$baseUrl}/index.php/index/admin" title="Admin">Admin</a></li>
		</ul>
		<ul class="header_nav right">
		</ul>
	</nav>

</header>

<div class="logo-wrapper">
	<div class="main-wrapper">
		<!-- Logo -->

		<div class="logo-main">
			<!--[if gt IE 8]><!-->

			<h1 id="logo">
				<a href="https://ub.unibas.ch" title="Universität Basel"><img src="{$baseUrl}/plugins/themes/defaultBasel/media/ci/uni-basel-logo-svg.svg" alt="Universität Basel"/></a>
			</h1>


			<!--<![endif]-->
			<!--[if (lt IE 9) & (!IEMobile)]>

			<h1 id="logo">
				<a href="https://ub.unibas.ch" title="Universität Basel"><img src="{$baseUrl}/plugins/themes/defaultBasel/media/ci/uni-basel-logo-png.png" alt="Universität Basel"/></a>
			</h1>


			<![endif]-->
			<!-- Department Title -->
			<h2><a href="https://ub.unibas.ch" title="Universitätsbibliothek">Universitätsbibliothek</a></h2>
		</div>


		{*
		<div class="logo-departement">
			<!--[if gt IE 8]><!-->

			<h1 id="logo-departement">
				<a href="#" title="Department"><img src="media/ci/dep-pharma-wissenschaften-logo-svg.svg" width="223" height="75" alt="Department"/>
			</h1>


			<!--<![endif]-->
			<!--[if (lt IE 9) & (!IEMobile)]>

			<h1 id="logo-departement">
				<a href="#" title="Department"><img src="media/ci/dep-pharma-wissenschaften-logo-png.png" width="223" height="75" alt="Department"/>
			</h1>


			<![endif]-->
		</div>*}



		<div class="clearfix"></div>
	</div>

	<!-- Fachbereich -->
	<div class="unit-wrapper">
		<div class="main-wrapper">
			{*<h2><a href="{$baseUrl}" title="eterna">eterna</a></h2>*}
			<h2>
				<a title="Home" href="{url page="index" router=$smarty.const.ROUTE_PAGE}" class="home">
					{$currentJournal->getLocalizedName()}
				</a>
			</h2>
		</div>
	</div>

	<!-- Mobile -->

	<!-- Navigation Tab -->
	<ul id="navigation-tab">
		<li><a data-target="navigation" title="Menü einblenden">Menü</a></li>
	</ul>

</div>
<!-- Navbar -->


	<section id="navigation-wrapper">
		<h6 class="hidden">Navigationen</h6>

		{if $enableAnnouncements}
			<li>
				<a href="{url router=$smarty.const.ROUTE_PAGE page="announcement"}">
					{translate key="announcement.announcements"}
				</a>
			</li>
		{/if}

		<!-- Main Navigation -->
		<nav class="navigation-lvl-1" id="main-nav">
			<h6 class="hidden">Hauptnavigation</h6>
			<ul>
				{if $currentJournal}

					{if $currentJournal->getSetting('publishingMode') != $smarty.const.PUBLISHING_MODE_NONE}
						<li>
							<a href="{url router=$smarty.const.ROUTE_PAGE page="issue" op="current"}">
								{translate key="navigation.current"}
							</a>
						</li>
						<li>
							<a href="{url router=$smarty.const.ROUTE_PAGE page="issue" op="archive"}">
								{translate key="navigation.archives"}
							</a>
						</li>
					{/if}

					<li>
						<a href="{url router=$smarty.const.ROUTE_PAGE page="about"}" data-target="sub-about">
							{translate key="navigation.about"}
						</a>
						<nav id="sub-about" class="section-group navigation-lvl-2">
							<ul>
								<li>
									<a href="{url router=$smarty.const.ROUTE_PAGE page="about"}">
										{translate key="about.aboutContext"}
									</a>
								</li>
								{if $currentJournal->getLocalizedSetting('editorialTeam')}
									<li>
										<a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="editorialTeam"}">
											{translate key="about.editorialTeam"}
										</a>
									</li>
								{/if}
								<li>
									<a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="submissions"}">
										{translate key="about.submissions"}
									</a>
								</li>
								{if $currentJournal->getSetting('mailingAddress') || $currentJournal->getSetting('contactName')}
									<li>
										<a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="contact"}">
											{translate key="about.contact"}
										</a>
									</li>
								{/if}
							</ul>
						</nav>
					</li>
					<li>
						<a href="{$baseUrl}" title="eterna">eterna</a>
					</li>
				{/if}
		</nav>

{*
{if !empty(trim($primaryMenu)) || $currentContext}
<nav class="pkp_navigation_primary_row" aria-label="{translate|escape key="common.navigation.site"}">
<div class="pkp_navigation_primary_wrapper">
{* Primary navigation menu for current application *}
				{*
				{$primaryMenu}

				{if $currentContext}
				*}
					{* Search form *}
					{*
					{include file="frontend/components/searchForm_simple.tpl"}
				{/if}
			</div>
		</nav>
	{/if}
	*}

	<nav class="pkp_navigation_user_wrapper" id="navigationUserWrapper" aria-label="{translate|escape key="common.navigation.user"}">
		{load_menu name="user" id="navigationUser" ulClass="pkp_navigation_user" liClass="profile"}
	</nav>

</section>

		{* Wrapper for page content and sidebars *}
		{if $isFullWidth}
			{assign var=hasSidebar value=0}
		{/if}
		<div class="pkp_structure_content{if $hasSidebar} has_sidebar{/if}">
			<div id="pkp_content_main" class="pkp_structure_main" role="main">
