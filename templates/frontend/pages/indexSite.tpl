{**
 * templates/frontend/pages/indexSite.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Site index.
 *
 *}

{assign var=journalsArray value=$journals->toArray()}
{include file="frontend/components/header_indexSite.tpl"}

<img src="public/titelbild.jpg" alt="Titelbild" width="6000" height="1317">

<div class="page_index_site">

	{if $about}
		<div class="about_site">
			{$about|nl2br}
		</div>
	{/if}

	<div class="journals">

		<h2>
			{translate key="journal.journals"}
		</h2>
		{if !count($journals)}
			{translate key="site.noJournals"}
		{else}

		   <div class="grid-container">
			{assign var="jindex" value=0}
			{foreach from=$journalsArray item=journal}
				{capture assign="url"}{url journal=$journal->getPath()}{/capture}
				{assign var="thumb" value=$journal->getLocalizedSetting('journalThumbnail')}
				{assign var="description" value=$journal->getLocalizedDescription()}
				{assign var="description_no_tag" value=$description|strip_tags}
				{assign var="modDesc" value=$descriptions[$jindex]}
				{assign var="jindex" value=$jindex+1}

				<div class="grid-item">

					{if $thumb}
						<div class="thumb">
						   <a href="{$url|escape}">
						      <img 
                                                         src="{$journalFilesPath}{$journal->getId()}/{$thumb.uploadName|escape:"url"}" 
						              {if $thumb.altText} alt="{$thumb.altText|escape}"{/if}  
                                                              title="{$journal->getLocalizedName()}" >
							</a>
						</div>
					{/if}
					<br/>

					<div class="body">
						<h3 class="indexSiteJ">
							<a href="{$url|escape}" rel="bookmark" class="indexSiteJ">
									{$journal->getLocalizedName()}
							</a>
						</h3>
						{if $modDesc}
							<div class="description">
								<a href="{$url|escape}" rel="bookmark" class="indexSiteJ">
									{$modDesc}
								</a>
							</div>
						{elseif $description_no_tag}
							<div class="description">
								<a href="{$url|escape}" rel="bookmark" class="indexSiteJignore">
									{$description_no_tag|truncate:120|spacify:"&shy;"|nl2br}
								</a>
							</div>
						{/if}
						<a href="{$url|escape}">{translate key="plugins.themes.defaultBasel.goto.publication"}</a> &nbsp;
						<a href="{url|escape journal=$journal->getPath() page="issue" op="current"}">{translate key="site.journalCurrent"}</a>
					</div>
				</div>  <!-- grid-item -->
			{/foreach}
	

		   </div>

		{/if}
	</div>

</div><!-- .page -->

{include file="frontend/components/footer.tpl"}
