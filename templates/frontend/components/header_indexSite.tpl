{**
 * lib/pkp/templates/frontend/components/header.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Common frontend site header.
 *
 * @uses $isFullWidth bool Should this page be displayed without sidebars? This
 *       represents a page-level override, and doesn't indicate whether or not
 *       sidebars have been configured for thesite.
 *}
{strip}
	{* Determine whether a logo or title string is being displayed *}
	{assign var="showingLogo" value=true}
	{if $displayPageHeaderLogo}
		{assign var="showingLogo" value=false}
	{/if}
{/strip}
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="de" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="de" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="de" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html lang="de" class="gt-ie8"> <![endif]-->
<html lang="{$currentLocale|replace:"_":"-"}" xml:lang="{$currentLocale|replace:"_":"-"}" class="no-js">
{if !$pageTitleTranslated}{capture assign="pageTitleTranslated"}{translate key=$pageTitle}{/capture}{/if}
{include file="frontend/components/headerHead.tpl"}

<body class="front pkp_page_{$requestedPage|escape|default:"index"} pkp_op_{$requestedOp|escape|default:"index"}{if $showingLogo} has_site_logo{/if}" dir="{$currentLocaleLangDir|escape|default:"ltr"}" id="top">

<!-- class="front" muss definiert sein damit die Mobile Navigation auf der ersten "Home" Seite funktioniert; auf Unterseiten: class="col-subcol" -->
<!-- Skip links -->
<div class="skip">
	<em>Springe direkt zu:</em>
	<ul class="navSkip">
		<li><a href="#page-content" class="">Inhalt</a></li>
		<li><a href="#main-nav">Hauptmenu</a></li>
		<li><a href="#page-footer">Footer</a></li>
	</ul>
</div>

<header class="main-header">
	<!-- Metanav -->
	<nav class="container-toggle content_wrapper">
		<h6 class="hidden">Meta Navigation</h6>
		<ul class="header_nav">
			{* change language *}
			{if isset($supportedLocales) && $supportedLocales|@count}
					{foreach from=$supportedLocales item=localeName key=localeKey}
						{if $localeKey != $currentLocale}
							<li>
								<a href="{url router=$smarty.const.ROUTE_PAGE page="user" op="setLocale" path=$localeKey}">
									{$localeName}
								</a>
							</li>
						{/if}
					{/foreach}
			{/if}
			{* admin panel *}
			<li><a href="{$baseUrl}/index.php/index/admin" title="Admin">Admin</a></li>
		</ul>
		<ul class="header_nav right">
		</ul>
	</nav>
</header>

<div class="logo-wrapper">
	<div class="main-wrapper">
		<!-- Logo -->

		<div class="logo-main">
			<!--[if gt IE 8]><!-->

			<h1 id="logo">
				<a href="https://ub.unibas.ch" title="Universität Basel"><img src="{$baseUrl}/plugins/themes/defaultBasel/media/ci/uni-basel-logo-svg.svg" alt="Universität Basel"/></a>
			</h1>


			<!--<![endif]-->
			<!--[if (lt IE 9) & (!IEMobile)]>

			<h1 id="logo">
				<a href="https://ub.unibas.ch" title="Universität Basel"><img src="{$baseUrl}/plugins/themes/defaultBasel/media/ci/uni-basel-logo-png.png" alt="Universität Basel"/></a>
			</h1>

			<![endif]-->
			<!-- Department Title -->
			<h2><a href="https://ub.unibas.ch" title="Universitätsbibliothek">Universitätsbibliothek</a></h2>
		</div>

		{* Add eterna logo here:
		<div class="logo-departement">
			<!--[if gt IE 8]><!-->

			<h1 id="logo-departement">
				<a href="#" title="Department"><img src="media/ci/dep-pharma-wissenschaften-logo-svg.svg" width="223" height="75" alt="Department"/>
			</h1>


			<!--<![endif]-->
			<!--[if (lt IE 9) & (!IEMobile)]>

			<h1 id="logo-departement">
				<a href="#" title="Department"><img src="media/ci/dep-pharma-wissenschaften-logo-png.png" width="223" height="75" alt="Department"/>
			</h1>


			<![endif]-->
		</div>
		*}

		<div class="clearfix"></div>
	</div>

	<!-- Fachbereich -->
	<div class="unit-wrapper">
		<div class="main-wrapper">
			<h2 style="padding-bottom: 0px;padding-top: 10px;">
				<div class="eterna_home">
					<a href="{$baseUrl}" title="eterna">{translate key="plugins.themes.defaultBasel.title"}</a>
				</div>
				<div class="emono_home">
                                       <a style="" title="eterna. Open Publishing Serials" 
                                          href="https://emono.unibas.ch/emono" >
                                               {translate key="plugins.themes.defaultBasel.emono.title"}
                                       </a>
                       		</div>
			</h2>
		</div>
	</div>

	<!-- Mobile -->

	<!-- Navigation Tab -->
	<ul id="navigation-tab">
		<li><a data-target="navigation" title="Menü einblenden">Menü</a></li>
	</ul>

</div>

<!-- Navbar -->
<section id="navigation-wrapper">
	<h6 class="hidden">Navigationen</h6>

	<!-- Main Navigation -->
	<nav class="navigation-lvl-1" id="main-nav">
		<h6 class="hidden">Hauptnavigation</h6>
		<ul>

			<li>
				<a href="{$baseUrl}" data-target="sub-zeitschriften">{translate key="plugins.themes.defaultBasel.publications"}</a>

				<nav id="sub-zeitschriften" class="section-group navigation-lvl-2">
					<h6 class="hidden">Subnavigation - Zeitschriften</h6>
					<ul>
						{* journalsArray defined in indexSite.tpl since $journals must be converted to array to be iterated more than once *}
						{foreach from=$journalsArray item=journal}
						<li>
							<a title="{$journal->getLocalizedName()}" class="ellipsis" href="{$baseUrl}/index.php/{$journal->getPath()}">{$journal->getLocalizedName()}</a>
						</li>
						{/foreach}

					</ul>
				</nav>

			</li>

			<li>
				<a href="{$baseUrl}/index.php/index/about" data-target="sub-uns">{translate key="plugins.themes.defaultBasel.about"}</a>
				{*
				<nav id="sub-uns" class="section-group navigation-lvl-2">
					<h6 class="hidden">Subnavigation - Über eterna</h6>
					<ul>
						<li>
							<a title="Custom site 1" class="ellipsis" href="{$baseUrl}/index.php/index/custom-1/">Custom site 1</a>
						</li>

						<li>
							<a title="Custom site 2" class="ellipsis" href="{$baseUrl}/index.php/index/custom-2/">Custom site 2</a>
						</li>

						<li>
							<a title="Custom site 3" class="ellipsis " href="{$baseUrl}/index.php/index/custom-3/">Custom site 3</a>
						</li>

					</ul>
				</nav>
				*}

			</li>

		</ul>
	</nav>

</section>

{* Wrapper for page content and sidebars *}
{if $isFullWidth}
	{assign var=hasSidebar value=0}
{/if}
<div class="pkp_structure_content{if $hasSidebar} has_sidebar{/if}">
<div id="pkp_content_main" class="pkp_structure_main" role="main">
