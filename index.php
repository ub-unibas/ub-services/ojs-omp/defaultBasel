<?php

/**
 * @defgroup plugins_themes_default_Basel Default theme plugin
 */

/**
 * @file plugins/themes/defaultBasel/index.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_themes_defaultBasel
 * @brief Wrapper for default Basel theme plugin.
 *
 */

require_once('DefaultBaselChildThemePlugin.inc.php');

return new DefaultBaselChildThemePlugin();

?>
