/*globals Modernizr:true */
/* Author: Jose */
(function($) {
	"use strict";

	$.widget('aperto.newsSocialMedia', {
		options: {
			// widget options
			isDebug: false
		},
		_create: function() {
			var that = this;
			this.$el = $(this.element[0]);
			this.selector = this.$el.data("sm-selector");
			this.url = this.$el.data("url");
			this.isDebug = this.options.isDebug;
			this.templates = [];
			this.isDebug && console.log("newsSocialMedia _initialize")

			this._getJsonObject();
		},
		_getJsonObject: function() {
			var that = this;

			if (window.magnoliaFrontendData === undefined) {
				var url = "data/" + this.selector + ".json";
			} else {
				var url = this.url ? this.url : "service/" + this.selector;
			}


			$.ajax({
				type: "GET",
				url: url,
				dataType: 'json',
				success: function(data, xhr, status) {
					that.isDebug && console.log("success", xhr);
					that._chooseSocialMedia(data);
				},
				error: function(error) {
					that.isDebug && console.log("error");
				}
			});
		},
		_chooseSocialMedia: function(data) {
			if (!this.templates[this.selector]) {
				this.templates[this.selector] = Handlebars.compile(window.templates[this.selector]);
			}
			this._render(this.templates[this.selector](data));
		},
		_render: function(feed) {
			this.$el.html(feed);
			this.$el.find("abbr.timeago").timeago();
		}
	});
})(window.webshims && webshims.$ || jQuery);
