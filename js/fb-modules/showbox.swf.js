/**
 * @author trixta
 */
(function($) {

	$.fn.showbox.defaults.hideContentAnim = function(ui) {
		$('div.content-box', ui.element).fadeTo(300, 0);
		ui.removeFlashContent();
	};

	$.ui.cOverlay.prototype.removeFlashContent = function() {
		var swf = $(this.element[0].getElementsByTagName('object'));
		if (swf[0] && this.options.swf.removeObject) {
			swf.after('<div style="height: ' + swf.height() + 'px; width: ' + swf.width() + 'px;" />');
			swfobject.removeSWF(swf.getID());
		}
	};

	$.fn.showbox.defaults.swf = {
		dims: {
			width: 600,
			height: 400
		},
		vars: {},
		params: {},
		attrs: {},
		expressInstall: null,
		removeObject: true
	};

	var swfReg = /\.swf$/i,
		uID = new Date().getTime();

	$.createUrlIndex.mmContent.add('swf', {
		filter: function(url, opener, urlPart) {
			if (opener.is('.swf, .flash')) {
				return true;
			}
			return (swfReg.test(urlPart));
		},
		load: function(url, opener, ui, fn) {
			var inst = ui.instance || ui,
				jElm = $([]),
				opts = inst.options.swf;

			var elmOpts = opener.data('showbox') || {},
				dims = $.extend({}, opts.dims),
				vars = $.extend(true, {}, opts.vars, elmOpts.vars || {}),
				params = $.extend(true, {}, opts.params, elmOpts.params || {}),
				attrs = $.extend({}, opts.attrs, elmOpts.attrs || {}),
				id = attrs.id || 'swfid-' + (uID++);

			attrs.name = attrs.name || id;

			if (elmOpts.height) {
				dims.height = parseInt(elmOpts.height, 10);
			}
			if (elmOpts.width) {
				dims.width = parseInt(elmOpts.width, 10);
			}

			if (ui.extras) {
				ui.extras.mm = jElm;
			}

			inst.content = {
				'multimedia-box': function(name, element, content, isClone) {
					var elem = $('<div></div>').css(dims);

					$('div.multimedia-box', element).html(elem);
					if (!isClone) {
						$('<div id="' + id + '" style="width: 100%;height: 100%;" />').appendTo(elem);
						if (ui.extras) {
							ui.extras.mm = elem;
						}

						swfobject.embedSWF(url, id, "100%", "100%", '9.0.124', opts.expressInstall, vars, params, attrs, function(swf) {
							if (swf.ref) {
								elem = $(swf.ref).css('visibility', '');
								if (ui.extras) {
									ui.extras.mm = elem;
								}
							}
						});
					}
				}
			};
			inst.content['multimedia-box'][0] = dims;
			$.extend(inst.content['multimedia-box'], dims);
			fn(url, dims.width);
		}
	});

})(window.webshims && webshims.$ || jQuery);
