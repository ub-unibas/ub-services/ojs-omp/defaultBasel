(function($) {
	var google;
	var isDebug = false,
		devmode = jspackager && jspackager.devmode;

	if (typeof window.aperto === "undefined") window.aperto = {};


	var projectInit = {
		immediate: function() {
			$.jme.startJME();
		},
		domReadyOnce: function() {
			window.HandlerbarsTemplates();
			// bindEventListenerToDocument();
		},
		everyDomReady: function(context) {

			// META NAV
			// initMetaNav(context);
			// do ellipsis
			doEllipsis(context);

			// setEqualHeights(context);

			// NEW NAVIGATION
			$('#navigation-wrapper', context).mainNav({
				onPanExpand: function(ui, panel) {
					// console.debug(arguments)
					Array.prototype.slice.apply($('#sublevel > ul > li > a', panel), []).forEach(function(item) {
						new Ellipsis(item, {});
					});
				}
			});

			// STAGE
			// $('.stage', context).each(function(idx) {
			// 	if ($(this).find('.stage-item').length > 1) {
			// 		$(this).carousel({
			// 			autoplay: true,
			// 			type: 'endless',
			// 			view: '.stage-wrapper',
			// 			doc: '.stage-area',
			// 			itemSel: '.stage-item > .stage-item-inner'
			// 		});
			// 	} else {
			// 		$(this).addClass('stage-single-item');
			// 	}
			// });

			// $('.faq_wrapper', context).each(function() {
			// 	var currentHash = location.hash || ('#' + jspackager.querys.id);
			// 	if ($(this).find(currentHash).length != 0) {
			// 		var top = $(this).find(currentHash).offset().top;
			// 		$.SCROLLROOT.animate({
			// 			scrollTop: top
			// 		});
			// 	}
			// });

			// initTabsCarousel(context);

			// initCarousel(context);

			initShowbox(context);

			// initTabTrees(context);

			// initSelectBox(context);

			// initSocialNetsShare(context);

			// initTeamListEvent(context);

			// formValidation(context);

			// initFaq(context);

			initSearchToggleBox(context);

			// makeSelectForMobile(context);

			// initOrganizationFinder(context);

			$('.file-input-wrapper', context).each(function() {
				var input = $(this).find('input[type=file]');
				var delBtn = $(this).find('.file-input-clear');
				var val = input[0].value;
				if (val === '') {
					input.removeClass('filled');
					delBtn.removeClass('js-visible');
				} else {
					input.addClass('filled');
					delBtn.addClass('js-visible');
				}

				delBtn.on('click', function() {
					input[0].value = '';
					input.removeClass('filled');
					delBtn.removeClass('js-visible');
				});
				input.on('change', function() {
					var val = $(this)[0].value;
					if (val === '') {
						input.removeClass('filled');
						delBtn.removeClass('js-visible');
					} else {
						input.addClass('filled');
						delBtn.addClass('js-visible');
					}
				});
			});

			// Search-Toggle In Meta Nav for Mobile & Desktop - is still 2 different MUs - HTML-Refactoring neccesary :)
			$('a[data-target="searchbar"], a[data-target="mobile-search"]', context).on('click', function(e) {
				e.preventDefault();
				var btn = $(this).closest('li');
				var pan = $('#' + $(this).data('target'));

				if (btn.is('.js-active')) {
					btn.removeClass('js-active');
					pan.removeClass('js-active');
					btn.setFocus();
				} else {
					btn.addClass('js-active');
					pan.addClass('js-active');
					$('input[type=search]', pan).setFocus();
				}
			});

			// PRINT
			$('#action_print a', context).on('click', function(e) {
				e.preventDefault();
				setTimeout(function() {
					window.print();
				}, 500);
			});

			$(".filter, #inpage-searchbar", context).searchModule();

			$(".footer_column.social_media", context).socialList();

			// $(".news-social-media .short", context).newsSocialMedia();

			// $('.audio-modul', context).audioModule();

			// $(".showbox-video", context).videoModule();

			$('#result-list li h4.toggle', context).each(function(idx) {
				$(this).on('click', function(e) {
					e.preventDefault();
					$(this).parent().find('a.toggle').trigger('click');
				});
			});

			// "EQUAL HEIGHT" for ALUMNI-TEASER
			$('.alumni-teaser a', context).each(function() {
				var pan1 = $(this).find('article header');
				var pan2 = $(this).find('article section');
				var h1 = pan1.outerHeight();
				var h2 = pan2.outerHeight();


				var height = (h1 > h2) ? h1 : h2;
				pan1.css({
					height: height + 20
				});
				pan2.css({
					height: height + 20
				});
			});

			// FOOTER "DROPDOWN"
			// var $footerLinks = $('.footer_column.links', context);
			//
			// if ((Modernizr.mq('(max-width: 767px)')) && $('nav > ul > li', $footerLinks).size() > 0) {
			// 	$footerLinks.tabtree({
			// 		panelSel: 'nav > ul',
			// 		buttonSel: 'h1',
			// 		toggleButton: true,
			// 		defaultSelected: -1
			// 	});
			// }

		},
		windowResize: function(context) {
			doEllipsis(context);
			// teamList(context);
			// setEqualHeights(context);
		}
	};

	var doEllipsis = function(context) {
		Array.prototype.slice.apply($('.ellipsis', context), []).forEach(function(item) {
			new Ellipsis(item, {});
		});
	};
	// var initTabTrees = function(context) {
	// 	var resultListOpts = {
	// 		buttonSel: 'a.toggle',
	// 		defaultSelected: -1,
	// 		toggleButton: true,
	// 		collapse: function(e, ui) {
	// 			ui.panel.stop().slideParentUp({
	// 				hideStyle: 'visibility'
	// 			});
	// 			$('.audio-modul', ui.panel).each(function() {
	// 				$(this).data('apertoAudioModule').disablePlayer();
	// 			})
	// 			$('.showbox-video', ui.panel).each(function() {
	// 				$(this).data('apertoVideoModule').disablePlayer();
	// 			});
	// 		},
	// 		init: function(e, ui) {
	// 			var currentHash = jspackager.querys.id;
	// 			ui.instance.panels.each(function(idx) {
	// 				var href = $(this).attr('id');
	// 				if (href === currentHash) {
	// 					$(this).data('tabtreepanel').expand();
	// 				}
	// 			});
	// 		},
	// 		expand: function(e, ui) {
	// 			ui.panel.stop().slideParentDown({
	// 				hideStyle: 'visibility'
	// 			});
	// 			setTimeout(function(e) {
	// 				//trigger Event so Locationfinder can listen to it
	// 				ui.panel.closest('li').trigger('panelExpanded', [{
	// 					'target': ui
	// 				}]);
	// 			}, 300);
	// 		},
	// 		hideStyle: 'visibility'
	// 	};
	// 	// Toggle with tabtree for Glossar & Orgafinder (T20 & T21)
	// 	$('#result-list', context).tabtree($.extend(resultListOpts, {
	// 		panelSel: '.expandable_content'
	// 	}));
	//
	// 	// Toggle with tabtree for Veranstaltungskalender (T19)
	// 	$('#date_entry_list', context).tabtree(resultListOpts);
	//
	//
	// 	$('ul.toc:not(.container_toggle)', context).tabtree();
	//
	//
	// 	// Toggle for Documents (T26)
	// 	var tabtreeOpts = {
	// 		buttonSel: '> li > div > h4 > a.toggle',
	// 		createPanelTabRelation: true,
	// 		defaultSelected: -1,
	// 		toggleButton: true,
	// 		activeButtonClass: 'on',
	// 		activePanelClass: 'open',
	// 		expand: function(e, data) {
	// 			var $panel = $(data.panel);
	//
	// 			if ($panel.is("[data-docs]")) {
	// 				$panel.documents();
	// 			}
	// 		},
	// 		init: function(e, ui) {
	// 			var currentHash = jspackager.querys.id;
	// 			ui.instance.panels.each(function(idx) {
	// 				var href = $(this).attr('id');
	// 				if (href === currentHash) {
	// 					$(this).data('tabtreepanel').expand();
	// 				}
	// 			});
	// 		}
	// 	};
	//
	// 	$('.documents ul.level-1', context).tabtree(tabtreeOpts);
	// 	$('.documents ul.level-2', context).tabtree(tabtreeOpts);
	// 	$('.documents ul.level-3', context).tabtree(tabtreeOpts);
	// 	$('.documents ul.level-4', context).tabtree(tabtreeOpts);
	// 	$('.documents ul.level-5', context).tabtree(tabtreeOpts);
	// 	$('.documents ul.level-6', context).tabtree(tabtreeOpts);
	//
	//
	// 	// Toggle for tabtree in Search
	// 	$('#global_search_result_list', context).tabtree({
	// 		buttonSel: 'a.toggle',
	// 		createPanelTabRelation: true,
	// 		defaultSelected: -1,
	// 		toggleButton: true,
	// 		activeButtonClass: 'on',
	// 		activePanelClass: 'open',
	// 		hideStyle: 'visibility',
	// 		expand: function panelSlide(e, ui) {
	// 			ui.panel.stop().slideParentDown({
	// 				hideStyle: 'visibility'
	// 			});
	// 		},
	// 		collapse: function panelSlide(e, ui) {
	// 			ui.panel.stop().slideParentUp({
	// 				hideStyle: 'visibility'
	// 			});
	// 		}
	// 	});
	// };

	// var initOrganizationFinder = function(context) {
	// 	$("nav ul.mobileselect", context).each(function() {
	// 		var select = '<select class="link_list">';
	//
	// 		$('li > a', this).each(function(idx, elem) {
	// 			select = select + '<option value="idx' + idx + '" data-href="' + elem.getAttribute('href') + '" ' + ($(elem).parent().is('.selected') ? 'selected' : '') + '>' + $(this).text() + '</option>';
	// 		});
	//
	// 		select = select + '</select>';
	//
	// 		$(select)
	// 			.insertAfter(this)
	// 			.on('change', function(event) {
	// 				window.location = $('option:selected', this).attr('data-href');
	// 			})
	// 			.combobox();
	// 	});
	//
	// 	$('.organisationsfinder .glossar form select', context).on('change', function(e) {
	// 		e.preventDefault();
	// 		$(this).closest('form').submit();
	// 	});
	// };

	// var initMetaNav = function(context) {
	// 	$('#introductions', context).each(function() {
	// 		$(this).on('click', function() {
	// 			var btn = $(this);
	// 			var pan = $('#' + btn.data('target'));
	//
	// 			if (pan.find('.close').length === 0) {
	// 				$('<a href="#" title="' + $.i18n.getText('close') + '" class="close toggle tablinks">' + $.i18n.getText('close') + '</a>')
	// 					.appendTo(pan.find('.content_wrapper'))
	// 					.on('click', function(e) {
	// 						e.preventDefault();
	// 						btn.removeClass('active');
	// 						pan.removeClass('is-visible').css({
	// 							height: 0
	// 						});
	// 						$('#locationfinder').data('apertoLocationfinderNew').closePanel();
	// 					});
	// 			}
	//
	// 			if (btn.is('.active')) {
	// 				btn.removeClass('active');
	// 				pan.removeClass('is-visible').css({
	// 					height: 0
	// 				});
	//
	// 			} else {
	// 				btn.addClass('active');
	// 				pan.addClass('is-visible').css({
	// 					height: pan.find('.content_wrapper').outerHeight() + 25
	// 				});
	// 				$('#locationfinder').data('apertoLocationfinderNew').closePanel();
	// 			}
	// 		});
	// 	});
	// };

	// var bindEventListenerToDocument = function() {
	// 	/*var onDocumentClick = function(event) {
	// 		isDebug && console.log("Click on document")
	// 		$('.dropdown_content').removeClass('open');
	// 	};
	//
	// 	$(document).on("click", onDocumentClick);*/
	//
	// 	$(document).bind('play', function(event) {
	// 		var $audioNoPlay = $('audio').not(event.target),
	// 			$videoNoPlay = $('video').not(event.target),
	// 			$iframes = $('.showbox-video.iframe').not(event.target);
	// 		$audioNoPlay.parent().parent().audioModule("disablePlayer");
	// 		$videoNoPlay.parent().videoModule("disablePlayer");
	// 		$iframes.videoModule("disablePlayer");
	// 	});
	// };

	// var initSocialNetsShare = function(context) {
	// 	$('#action_share', context).tabtree({
	// 		buttonSel: '>a',
	// 		createPanelTabRelation: true,
	// 		defaultSelected: -1,
	// 		toggleButton: true
	// 	});
	//
	// 	var socialOptions = {
	// 		'services': {
	// 			'facebook': {
	// 				'status': 'on',
	// 				'dummy_img': ''
	// 			},
	// 			'twitter': {
	// 				'status': 'on',
	// 				'dummy_img': ''
	// 			},
	// 			'gplus': {
	// 				'status': 'on',
	// 				'dummy_img': ''
	// 			},
	// 			'img_path': ''
	// 		}
	// 	};
	//
	// 	$("#socialshareprivacy", context).socialSharePrivacy(socialOptions);
	//
	// 	$('#socialshareprivacy', context).tabtree({
	// 		buttonSel: '> a.toggle',
	// 		createPanelTabRelation: true,
	// 		defaultSelected: -1,
	// 		toggleButton: true,
	// 		expand: function(e, ui) {
	// 			ui.panel.find('a.toggle').on('click', function(e) {
	// 				e.preventDefault();
	// 				ui.panel.data('tabtreepanel').collapse();
	// 			});
	// 		}
	// 	});
	//
	// };

	// var initTeamListEvent = function(context) {
	// 	var $teamListElements = $(".teamlist.closed li", context);
	//
	// 	$teamListElements.each(function(index) {
	// 		$(this).css("z-index", $teamListElements.size() - index);
	// 	});
	// 	/*-
	// 	 * initialize container toggles
	// 	 -*/
	// 	/*new aperto.ContainerToggle({
	// 		selector: ".toggle.icon.handle",
	// 		context: ".teamlist",
	// 		toggleHandle: ".toggle-handle",
	// 		expandable: ".teamprofile.additional",
	// 		autoHeight: true,
	// 		enableAria: true,
	// 		isDebug: false
	// 	});*/
	//
	// 	$('ul.teamlist.closed', context).tabtree({
	// 		panelSel: 'li .teamprofile.additional',
	// 		buttonSel: 'li a.toggle.handle',
	// 		toggleButton: true,
	// 		defaultSelected: -1,
	// 		expand: function(e, ui) {
	// 			ui.panel.find('a.toggle').on('click', function(e) {
	// 				e.preventDefault();
	// 				ui.panel.data('tabtreepanel').collapse();
	// 			});
	// 		}
	// 	});
	// };

	// var initFaq = function(context) {
	// 	$('.faq_wrapper:not(.small) .faq_list', context).each(function(e) {
	// 		var that = this;
	// 		var faqElms = $('li', that);
	//
	// 		var btns = $('.toggle_faq', that);
	// 		$('.question', faqElms).removeClass('js-hidden');
	// 		$('.answer', faqElms).addClass('js-hidden');
	//
	// 		btns.on('click', function(e) {
	// 			var parent = $(this).closest('li');
	//
	// 			if ($(parent).is('.active')) {
	// 				$(parent).removeClass('active');
	// 				$(this).removeClass('active');
	// 				$('.question', parent).removeClass('js-hidden');
	// 				$('.answer', parent).addClass('js-hidden');
	// 			} else {
	// 				faqElms.removeClass('active');
	// 				$('.question', faqElms).removeClass('js-hidden');
	// 				$('.answer', faqElms).addClass('js-hidden');
	// 				$('.toggle_faq', faqElms).removeClass('active');
	//
	// 				$(parent).addClass('active');
	// 				$(this).addClass('active');
	// 				$('.question', parent).addClass('js-hidden');
	// 				$('.answer', parent).removeClass('js-hidden');
	// 			}
	// 		});
	// 	});
	// 	//FAQ SLIDER
	// 	$('.faq_wrapper.small .faq_list', context).each(function(e) {
	// 		var that = this;
	// 		var btn = $('.toggle_faq', that);
	// 		var question = $('.question', that);
	// 		var answer = $('.answer', that).addClass('js-hidden');
	//
	// 		btn.on('click', function(e) {
	// 			if ($(this).is('.active')) {
	// 				$(this).removeClass('active');
	// 				question.removeClass('js-hidden');
	// 				answer.addClass('js-hidden');
	// 			} else {
	// 				$(this).addClass('active');
	// 				question.addClass('js-hidden');
	// 				answer.removeClass('js-hidden');
	// 			}
	//
	// 		});
	// 	});
	// 	$('.faq_wrapper.small', context).carousel({
	// 		view: '.faq_flexslider',
	// 		doc: '.slides',
	// 		itemSel: '.slide > .faq_list',
	// 		paginationGrouping: false
	// 	});
	// };

	// var formValidation = function(context) {
	// 	var $form = $('form.validate', context);
	// 	if ($form.length)
	// 		$form.parsley();
	// };

	// var initTabsCarousel = function(context) {
	// 	$(".news_social_media", context).tabsCarousel();
	// 	$(".calender", context).tabsCarousel();
	// };

	// var initCarousel = function(context) {
	//
	// 	$('.carousel:not(.has-thumbview, .is-thumbview)', context).carousel();
	//
	// 	$('.carousel.has-thumbview', context).carousel({
	// 		hasThumbView: true
	// 	});
	// 	$('.carousel.is-thumbview', context).carousel({
	// 		paginationGrouping: true,
	// 		isThumbView: true,
	// 		viewportBreaks: {
	// 			800: 4
	// 		}
	// 	});
	// };


	var initShowbox = function(context) {

		var showboxOpts = {
			structure: '<div class="showbox">' +
				'<div class="showbox-box">' +
				'<div class="showbox-head">' +
				'</div>' +
				'<div class="content-box">' +
				'<div class="multimedia-box-wrapper">' +
				'<span class="prev overlay-control over-control"><span /></span>' +
				'<span class="next overlay-control over-control"><span /></span>' +
				'<div class="multimedia-box"></div>' +
				'</div>' +
				'<div class="showbox-footer">' +
				'<span class="showbox-toolbar">' +
				'<span class="current-index" /> / <span class="item-length" />' +
				'</span>' +
				'<div class="showbox-info"></div>' +
				'</div>' +
				'<div class="text-content"></div>' +
				'</div>' +
				'<a role="button" class="close-button" href="#">' + $.i18n.getText('close') + '</a>' +
				'</div>' +
				'</div>',
			widthElementSel: '.showbox',
			beforeShow: function(jElm, ui) {
				if ((Modernizr.mq('(max-width: 767px)'))) {
					ui.instance.hide();
				};
			},
			animShow: function(jElm, ui) {
				if ((Modernizr.mq('(max-width: 767px)'))) {
					ui.instance.hide();
				} else {
					delete ui.posCSS.height; //Höhe löschen wird automatisch durch den content bestimmt
					jElm.css(ui.posCSS).css({
						visibility: 'visible'
					});
				}
			},
			showContentAnim: function(ui, img, e, extras) {
				if ((Modernizr.mq('(max-width: 767px)'))) {
					ui.instance.hide();
				} else {
					var contentBox = $('div.content-box', ui.element),
						duration = 300;

					contentBox
						.queue(function() {
							var oldDim = {
								width: ui.element.width(),
								height: ui.element.height()
							};
							var newDim;
							var pos;
							ui.fillContent();
							ui.element.css({
								width: ui.calcWidth(img, $(img[0]).width())
							});
							pos = $.ui.cOverlay.posMethods.constrainHorizontalView(ui.element, e, {
								mm: img
							}, ui);
							newDim = {
								width: ui.element.width(),
								height: ui.element.height()
							};

							ui.element
								.css(oldDim)
								.animate($.extend({}, pos, newDim), {
									duration: duration,
									complete: function() {
										contentBox.fadeTo(300, 1);
										ui.element.css({
											height: ''
										});
									}
								});
							contentBox.dequeue();
						});
				}
			},
			show: function(jElm, ui) {
				if ((Modernizr.mq('(max-width: 767px)'))) {
					ui.instance.hide();
				}
			},
			positionType: 'constrainHorizontalView'
		};
		if (!(Modernizr.mq('(max-width: 767px)'))) {

			// EACH CAROUSEL FOR ITS SELF
			$('.carousel', context).each(function(idx) {
				$(':not(.clone) .resize', this).showbox($.extend(showboxOpts, {
					getTextContent: function(opener, content, ui) {
						var elem = $(opener.closest('li'));
						content['showbox-info'] = elem.find('.description').html();
					},
					extraClass: 'gallery-lightbox'
				}));
			});
			// EACH SINGLE IMAGE FOR ITS SELF
			$('.single_showbox .resize', context).each(function(idx) {
				$(this).showbox($.extend(showboxOpts, {
					getTextContent: function(opener, content, ui) {
						var elem = $(opener.closest('.single_showbox'));
						content['showbox-info'] = elem.find('.description').html();
					},
					extraClass: 'single-lightbox'
				}));
			});
			// MEDIA - DB
			$('.media-container .image_wrapper', context).showbox(
				$.extend(showboxOpts, {
					getTextContent: function(opener, content, ui) {
						var elem = $(opener.closest('.media-wrapper'));
						content['showbox-head'] = elem.find('h1').text();
						content['text-content'] = elem.find('.media-info').html();
					},
					extraClass: 'media-db'
				})
			);
		} else {
			$('.resize', context).on('click', function(e) {
				e.preventDefault();
			})
		}
	};

	// var teamList = function(context) {
	// 	$('.teamlist.open', context).each(function() {
	// 		$(this).setTeamListEqualHeight();
	// 	});
	// };

	// var setEqualHeights = function(context) {
	// 	isDebug && console.log("_setEqualHeights");
	//
	// 	var classArray = [
	// 		".newsbox_list",
	// 		".teamlist.closed",
	// 		".teamlist.open"
	// 	];
	//
	//
	// 	$(".newsbox_list li", context).setEqualHeight();
	// 	// teamlist element
	//
	//
	// 	$(".content_wrapper.double-case .column", context).setEqualHeight();
	// 	// magazin site
	// 	if (!(Modernizr.mq('(max-width: 767px)'))) {
	// 		$(".tiles_list", context).each(function(index, item) {
	// 			$("> li", item).setEqualHeight();
	// 		});
	// 	}
	//
	// 	$(".editionsbox li", context).setEqualHeight();
	//
	// 	$(".teamlist.closed", context).equalRowHeight({
	// 		childElements: '.teamlist-item',
	// 		addPadding: 50
	// 	});
	//
	// 	$(".media-container", context).equalRowHeight({
	// 		childElements: '.media-wrapper'
	// 	});
	//
	// 	$("ul.link_list.content_full", context).equalRowHeight();
	//
	// 	$(".js-is-equal", context).equalRowHeight({
	// 		childElements: $(this).attr('data-is-equal-child')
	// 	});
	//
	// 	$(".person-teaser", context).equalRowHeight({
	// 		childElements: '.person-teaser-item'
	// 	});
	//
	// 	$(document, context).on("panelExpanded", function(e, element) {
	// 		$('.link_list', element.target.panel[0]).equalRowHeight();
	// 	});
	//
	// 	teamList(context);
	// };

	// var initSelectBox = function(context) {
	// 	$("div.select-box-wrapper", context).inputHandler({
	// 		masterSelectBoxId: "col_main_categories"
	// 	});
	// };

	var initSearchToggleBox = function(context) {

		new aperto.ContainerToggle({
			selector: "a.filter-switch",
			activeClass: "on",
			context: ".filter-switch-container",
			expandable: ".main_expandable_content",
			autoHeight: true,
			initialTab: 0,
			speed: 300
		});

		new aperto.ContainerToggle({
			selector: "a.toggle",
			activeClass: "on",
			context: ".filter-wrapper",
			deviceAdaptedTargets: true,
			expandable: ".expandable_content",
			autoHeight: true,
			speed: 300
		});

		var defaultSelected = 0;
		if ((Modernizr.mq('(max-width: 767px)'))) {
			defaultSelected = -1;
		}

		// $('.filter-switch-container', context).stickyFilter();
	};

	// var makeSelectForMobile = function(context) {
	// 	$('ul.link_list.blue_bg', context).each(function(idx) {
	//
	// 		var select = '<select class="link_list">';
	//
	// 		$('li > a', this).each(function(idx) {
	// 			select = select + '<option data-href="' + $(this).attr('href') + '" value="idx' + idx + '">' + $(this).text() + '</option>';
	// 		});
	//
	// 		select = select + '</select>';
	//
	// 		$(select)
	// 			.insertAfter(this)
	// 			.on('change', function() {
	// 				window.location.pathname = $(this.selectedOptions).attr('data-href');
	// 			});
	// 		setTimeout(function() {
	// 			$('select.link_list').combobox();
	// 		}, 100);
	// 	});
	// };

	if (!Date.prototype.toISOString) {
		(function() {

			function pad(number) {
				var r = String(number);
				if (r.length === 1) {
					r = '0' + r;
				}
				return r;
			}

			Date.prototype.toISOString = function() {
				return this.getUTCFullYear() +
					'-' + pad(this.getUTCMonth() + 1) +
					'-' + pad(this.getUTCDate()) +
					'T' + pad(this.getUTCHours()) +
					':' + pad(this.getUTCMinutes()) +
					':' + pad(this.getUTCSeconds()) +
					'.' + String((this.getUTCMilliseconds() / 1000).toFixed(3)).slice(2, 5) +
					'Z';
			};
		}());
	}

	/* starters */
	projectInit.immediate();
	$(projectInit.domReadyOnce);
	$(function() {
		projectInit.everyDomReady(document);

		$(document).on('dommodified', function(e) {
			projectInit.everyDomReady(e.target);
		});

		$(window).on('resize', $.Aperto.throttle(function(e) {
			projectInit.windowResize(document);
		}, 200));
	});

})(window.webshims && webshims.$ || jQuery);
