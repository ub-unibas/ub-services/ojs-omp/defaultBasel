(function($) {
	/*DEUTSCH*/
	$.i18n.setText('de', {
		close: 'schliessen',
		open: 'öffnen',
		carousel: {
			link: 'link'
		},
		maps: {
			linkText: 'in Google Maps'
		},
		autocomplete: {
			noResults: "No search results.",
			results: function(amount) {
				return amount + (amount > 1 ? " results are" : " result is") +
					" available, use up and down arrow keys to navigate.";
			}
		},
		isPlaying: 'Stoppen',
		isPausing: 'Abspielen',
		twitter: 'Auf Twitter folgen',
		facebook: 'Auf Facebook folgen',
		instagram: 'Auf Instagram folgen',
		youtube: 'Auf Youtube folgen',
		timeAgoStrings: {
			prefixAgo: "vor",
			prefixFromNow: "in",
			suffixAgo: "",
			suffixFromNow: "",
			seconds: "wenigen Sekunden",
			minute: "etwa einer Minute",
			minutes: "%d Minuten",
			hour: "etwa einer Stunde",
			hours: "%d Stunden",
			day: "etwa einem Tag",
			days: "%d Tagen",
			month: "etwa einem Monat",
			months: "%d Monaten",
			year: "etwa einem Jahr",
			years: "%d Jahren"
		},
		socialShare: {
			txt_help: 'Wenn Sie diese Felder durch einen Klick aktivieren, werden Informationen an Facebook, Twitter oder Google in die USA &uuml;bertragen und unter Umst&auml;nden auch dort gespeichert. N&auml;heres erfahren Sie <a href="http://www.heise.de/ct/artikel/2-Klicks-fuer-mehr-Datenschutz-1333879.html" title="mehr Informationen" target="_blank">hier</a>.',
			settings_perma: 'Dauerhaft aktivieren und Daten&uuml;ber&shy;tragung zustimmen:',
			settings: 'Einstellungen',
			hint: 'Hinweis'
		},
		errorMsg: "Es ist ein Fehler aufgetreten. Bitte probieren Sie es später erneut.",
		visit: "Zur Website",
		openNewTab: 'Link öffnet neuen Tab',
		more: 'mehr erfahren'

	});
	/*ENGLISH*/
	$.i18n.setText('en', {
		close: 'close',
		open: 'open',
		carousel: {
			link: 'link'
		},
		maps: {
			linkText: 'in Google Maps'
		},
		isPlaying: 'Stop',
		isPausing: 'Play',
		twitter: 'Connect on Twitter',
		facebook: 'Connect on Facebook',
		instagram: 'Connect on Instagram',
		youtube: 'Connect on Youtube',
		timeAgoStrings: {
			prefixAgo: null,
			prefixFromNow: null,
			suffixAgo: "ago",
			suffixFromNow: "from now",
			seconds: "less than a minute",
			minute: "about a minute",
			minutes: "%d minutes",
			hour: "about an hour",
			hours: "about %d hours",
			day: "a day",
			days: "%d days",
			month: "about a month",
			months: "%d months",
			year: "about a year",
			years: "%d years",
			wordSeparator: " ",
			numbers: []
		},
		socialShare: {
			txt_help: 'With Enabling Information will be transmitted and might be stored to Facebook, Twitter and Google+ For Further Information please click <a href="http://www.heise.de/ct/artikel/2-Klicks-fuer-mehr-Datenschutz-1333879.html" title="more Informationen" target="_blank">here</a>.',
			settings_perma: 'Activate Datatransfer permanently:',
			settings: 'Settings',
			hint: 'Hint'
		},
		errorMsg: "An Error occured. Please try again Later.",
		visit: "To Website",
		openNewTab: 'Link opens in new Tab',
		more: 'more'
	});
	$.i18n.setDefault('de');
})(jQuery);
