<?php

/**
 * @file plugins/themes/default/DefaultBaselChildThemePlugin.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class DefaultBaselChildThemePlugin
 * @ingroup plugins_themes_default_basel
 *
 * @brief Default theme
 */
import('lib.pkp.classes.plugins.ThemePlugin');

class DefaultBaselChildThemePlugin extends ThemePlugin {
	/**
	 * Initialize the theme's styles, scripts and hooks. This is only run for
	 * the currently active theme.
	 *
	 * @return null
	 */
	public function init() {

		// Initialize template manager
		HookRegistry::register ('TemplateManager::display', array($this, 'loadTemplateData'));


		// Initialize the parent theme
		$this->setParent('defaultthemeplugin');


		// Add custom styles
		$this->modifyStyle('stylesheet', array('addLess' => array('styles/index.less')));


		// Add Unibas css
		$this->addStyle('basel-css-icons', 'css/main.icons.css');


		// Add Unibas javascript
		$this->addScript('__basic-behaviour', 'js/__basic-behaviour.js');
		$this->addScript('__base-modules', 'js/__base-modules.js');
		$this->addScript('__base-modules2', 'js/__base-modules2.js');
		$this->addScript('__enhanced-modules', 'js/__enhanced-modules.js');


		// Remove the typography options of the parent theme.
		// `removeOption` was introduced in OJS 3.0.2
		if (method_exists($this, 'removeOption')) {
			$this->removeOption('typography');
		}


		// Add the option for an accent color
		$this->addOption('accentColour', 'FieldColor', [
			'label' => __('plugins.themes.defaultBasel.option.accentColour.label'),
			'description' => __('plugins.themes.default.option.colour.description'),
			'default' => '#F7BC4A',
		]);

        /**
		// Load the Montserrat and Open Sans fonts
		$this->addStyle(
			'font',
			'//fonts.googleapis.com/css?family=Montserrat:400,700|Noto+Serif:400,400i,700,700i',
			array('baseUrl' => '')
		);
        */
		// Dequeue any fonts loaded by parent theme
		// `removeStyle` was introduced in OJS 3.0.2
		if (method_exists($this, 'removeStyle')) {
			$this->removeStyle('fontNotoSans');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSansNotoSerif');
			$this->removeStyle('fontLato');
			$this->removeStyle('fontLora');
			$this->removeStyle('fontLoraOpenSans');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
		}

		// Start with a fresh array of additionalLessVariables so that we can
		// ignore those added by the parent theme. This gets rid of @font
		// variable overrides from the typography option
		$additionalLessVariables = array();

		/**
		// Update colour based on theme option from parent theme
		if ($this->getOption('baseColour') !== '#1E6292') {
			$additionalLessVariables[] = '@bg-base:' . $this->getOption('baseColour') . ';';
			if (!$this->isColourDark($this->getOption('baseColour'))) {
				$additionalLessVariables[] = '@text-bg-base:rgba(0,0,0,0.84);';
			}
		}

		// Update accent colour based on theme option
		if ($this->getOption('accentColour') !== '#F7BC4A') {
			$additionalLessVariables[] = '@accent:' . $this->getOption('accentColour') . ';';
		}

		if ($this->getOption('baseColour') && $this->getOption('accentColour')) {
			$this->modifyStyle('stylesheet', array('addLessVariables' => join('', $additionalLessVariables)));
		}
        */
	}

	/**
	 * Get the display name of this plugin
	 * @return string
	 */
	function getDisplayName() {
		return __('plugins.themes.defaultBasel.name');
	}

	/**
	 * Get the description of this plugin
	 * @return string
	 */
	function getDescription() {
		return __('plugins.themes.defaultBasel.description');
	}

	// Add $journals variable to theme
	/**
	 * Fired when the `TemplateManager::display` hook is called.
	 *
	 * @param string $hookname
	 * @param array $args [$templateMgr, $template, $sendContentType, $charset, $output]
	 */
	public function loadTemplateData($hookName, $args) {

		// Retrieve the TemplateManager
		$templateMgr = $args[0];

		$journalDao = DAORegistry::getDAO('JournalDAO'); /* @var $journalDao JournalDAO */
		$journals = $journalDao->getAll(true);

		$template = $args[1];
		if ($template == 'frontend/pages/indexSite.tpl') {
			$my_js = $journalDao->getAll(true);
			$search_ms = ["\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x98", "\xe2\x80\x99"];
			$replace_ms = ['"', '"', "'", "'"];
			$journalsArray = $my_js->toArray();
			$descArray = array();
			foreach ($journalsArray as $j) {
				$thisDesc = $j->getLocalizedDescription();
				$thisDesc = strip_tags($thisDesc);
				$origLen = strlen($thisDesc);
				$thisDesc = substr($thisDesc, 0, 120);
//error_log("before[".$thisDesc."]\n");
				if ($origLen>120) {
					$thisDesc = $thisDesc.'...';
				}
				$words = explode(' ', $thisDesc);
				$modWords = array();
				foreach ($words as $word) {
					$word = str_replace($search_ms, $replace_ms, $word);
					// use a utf-8 safe version of wordwrap (i.e. the /u modifier on the regex)
					$width = 4;
					$break = "&shy;";
					$search_w = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
					$replace_w = '$1$2'.$break;
					$word = preg_replace($search_w, $replace_w, $word);
					//$word = wordwrap($word, 4, "&shy;", true);
					$modWords[] = $word;
				}
				$modDesc = implode(' ', $modWords);
				$modDesc = nl2br($modDesc);
//error_log("after[".$modDesc."]\n");
				$descArray[] = $modDesc;
			}
			$templateMgr->assign('journals', $journals);
			$templateMgr->assign('descriptions', $descArray);
		} else {
			$templateMgr->assign('journals', $journals);
		}
	}


}

?>
