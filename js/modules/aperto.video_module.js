/*globals Modernizr:true */
/* Author: Jose */
(function($) {
	"use strict";

	$.widget('aperto.videoModule', {
		options: {
			// widget options
			isDebug: false,
			mobileWidth: 768,
			videoIFrame: "video-iframe",
			iconPlay: ".icon_wrapper",
			fullscreen: ".fullscreen"
		},
		_create: function() {
			var that = this,
				o = this.options;

			this.$el = $(this.element[0]);
			this.href = this.$el.attr("href");
			this.poster = this.$el.find("img").attr("src");
			this.isIFrame = this.$el.is(".iframe");
			this.$icon = $(o.iconPlay, this.$el);
			this.$player = $("video", this.$el);
			this.$handle = this.isIFrame ? this.$el : this.$player;

			this.videoIFrame = Handlebars.compile(window.templates[o.videoIFrame]);

			this._createIFrame();

			this.isDebug = o.isDebug;
			this.isDebug && console.log("showboxVideo _create");

			this._bindEvents(this, o);
			this._initialize();
		},
		_initialize: function() {
			this._resizePlayer();
			this._bindClickEvent();
		},
		_bindEvents: function(that, o) {
			$(window).smartresize(this._onResizeActions.bind(this));

			$(o.fullscreen, this.$el).on("click", function(event) {
				event.preventDefault();
				that.isFullscreen = true;
				return true;
			})
		},
		_bindClickEvent: function() {
			this.isDebug && console.log("_bindClickEvent");
			this.$handle.on("click", this._onClickEvent.bind(this));
		},
		_onResizeActions: function(event, data) {
			this.isDebug && console.log("_onResizeActions", this);

			if (this.isFullscreen) {
				this.isFullscreen = false;
			} else {
				this.disablePlayer();
				this._resizePlayer();
			}
		},
		_resizePlayer: function() {
			if (this.isIFrame) return;

			var $img = $("img", this.$el);
			// video tag as big as the poster pic
			this.$player.width($img.width());
			this.$player.height($img.height());
			this.$el.jmeProp("controlbar", false);
		},
		_onClickEvent: function(event) {
			this.isDebug && console.log("_onClickEvent");
			event.preventDefault();

			if (this.isIFrame) this._createIFrame();
			this._enablePlayer();
		},
		_enablePlayer: function() {
			if (!this.isIFrame) {
				this.isDebug && console.log("_enablePlayer video");
				this.$icon.hide();
				this.$player.play();
				this.$el.jmeProp("controlbar", true);
			} else {
				this.isDebug && console.log("_enablePlayer iframe");
				this.$el.addClass("hide");
				this._insertIFrame();
				this.$el.trigger("play");
			}
		},
		disablePlayer: function() {
			if (!this.isIFrame) {
				this.isDebug && console.log("disablePlayer video");
				this.$icon.show();
				this.$player.pause();
				this.$el.jmeProp("controlbar", false);
			} else {
				this.isDebug && console.log("disablePlayer iframe");
				this.$el.removeClass("hide");
				if (typeof this.$video !== "undefined") this.$video.remove();
			}
		},
		_createIFrame: function() {
			var $img = this.$el.find("img");

			this.iFrame = this.videoIFrame({
				width: $img.width(),
				height: $img.height(),
				poster: this.poster,
				href: this.href
			});
		},
		_insertIFrame: function() {
			this.$video = $(this.iFrame);
			this.$el.after(this.$video);
		}
	});
})(window.webshims && webshims.$ || jQuery);
