{**
 * templates/frontend/components/navigationMenu.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Primary navigation menu list for OJS
 *
 * @uses navigationMenu array Hierarchical array of navigation menu item assignments
 * @uses id string Element ID to assign the outer <ul>
 * @uses ulClass string Class name(s) to assign the outer <ul>
 * @uses liClass string Class name(s) to assign all <li> elements
 *}

{if $navigationMenu}
	<nav class="navigation-lvl-1" id="main-nav">
		<h6 class="hidden">Hauptnavigation</h6>
		<ul>
			{foreach key=field item=navigationMenuItemAssignment from=$navigationMenu->menuTree}
				{if !$navigationMenuItemAssignment->navigationMenuItem->getIsDisplayed()}
					{continue}
				{/if}
				<li>
					{* Match match href data-target and nav id with $menumatch variable; getTitle() does not work *}
					{$menumatch="sub-{$navigationMenuItemAssignment->navigationMenuItem->getLocalizedTitle()}"}
					{* Replace whitespaces with "-" for JS to work *}
					{$menumatch= str_replace(' ', '-', $menumatch)}
					<a href="{$navigationMenuItemAssignment->navigationMenuItem->getUrl()}" data-target="{$menumatch}">
						{$navigationMenuItemAssignment->navigationMenuItem->getLocalizedTitle()}
					</a>
					{if $navigationMenuItemAssignment->navigationMenuItem->getIsChildVisible()}
					<nav id="{$menumatch}" class="section-group navigation-lvl-2">
						<ul>

							{foreach key=childField item=childNavigationMenuItemAssignment from=$navigationMenuItemAssignment->children}
								{if $childNavigationMenuItemAssignment->navigationMenuItem->getIsDisplayed()}
									<li class="{$liClass|escape}">
										<a href="{$childNavigationMenuItemAssignment->navigationMenuItem->getUrl()}">
											{$childNavigationMenuItemAssignment->navigationMenuItem->getLocalizedTitle()}
										</a>
									</li>
								{/if}
							{/foreach}
						</ul>
					</nav>
					{/if}
				</li>
			{/foreach}
		</ul>
	</nav>
{/if}