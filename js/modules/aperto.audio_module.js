(function($) {
	"use strict";

	/*-
	 * custom audio modul
	 * initialize from 1 to n audio players, from which
	 * only one would play at a time
	-*/
	$.widget("aperto.audioModule", {
		options: {
			isDebug: false,
			audioPlayer: ".mediaplayer audio",
			audioControl: ".jme-default-control-bar",
			activeClass: "play",
			audioModuleContent: "div.audio-modul-content",
			contentPlay: "div.content-play"
		},
		/*-
		 * query the important elements from this module and
		 * bind the event listeners 
		-*/
		_create: function() {
			this.isDebug = this.options.isDebug;
			this.isDebug && console.info("initialize ", this);
			this.$el = $(this.element);
			this.$player = $(this.options.audioPlayer, this.element);
			this.$audioControl = $(this.options.audioControl, this.$el);
			this.$audioModuleContent = $(this.options.audioModuleContent, this.$el);

			this._bindEventListeners();
			var that = this;


			setTimeout(function() {
				var durationElement = $('.audio-date', that.$el);
				var audioDuration = that.$audioControl.find('.duration-display').text();
				if ($.trim($(durationElement).html()) == '') {
					$('<span class="duration"> ' + audioDuration + ' MIN</span>').appendTo(durationElement);
				} else {
					$('<span class="duration">' + audioDuration + ' MIN</span>').appendTo(durationElement);
				}
			}, 1500);
		},
		/*-
		 * bind event listeners:
		 ** on player start: play audio
		 ** on player ended	
		-*/
		_bindEventListeners: function() {
			var that = this;

			$('.start-player', this.$el).on('click', function(event) {
				that.isDebug && console.log('startPlayer');
				that.$player.play();
				event.preventDefault();

				that._enablePlayer(event);
			});

			this.$player
				.on("ended", this.disablePlayer.bind(this));
		},
		/*-
		 * show the audio player
		-*/
		_enablePlayer: function(event) {
			// if the audio player is already active, will not be activated
			if (this.$audioModuleContent.hasClass(this.options.activeClass))
				return;

			this.isDebug && console.log("_enablePlayer");

			this.$audioModuleContent.addClass(this.options.activeClass);
			this.$audioControl.addClass(this.options.activeClass);
		},
		/*-
		 * hide the audio player
		-*/
		disablePlayer: function(event) {
			this.isDebug && console.log("disablePlayer")

			this.$audioModuleContent.removeClass(this.options.activeClass);
			this.$audioControl.removeClass(this.options.activeClass);
			this.$player.load();
		}
	});

})(window.webshims && webshims.$ || jQuery);
