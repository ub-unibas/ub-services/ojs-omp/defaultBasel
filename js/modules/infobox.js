// Modified by Jose Luis Medina Burgos

/* An InfoBox is like an info window, but it displays
 * under the marker, opens quicker, and has flexible styling.
 * @param {GLatLng} latlng Point to place bar at
 * @param {Map} map The map on which to display this InfoBox.
 * @param {Object} opts Passes configuration options - content,
 *   offsetVertical, offsetHorizontal, className, height, width
 */
function InfoBox(opts) {
	this.defaults = {
		height: 400,
		Width: 300
	}

	this.options = $.extend(this.defaults, opts);
	var o = this.options;

	google.maps.OverlayView.call(this);
	this.latlng = o.latlng;
	this.map = o.map;
	this.height = o.height;
	this.width = o.width;
	this.offsetVertical = -this.height - 70;
	this.offsetHorizontal = -(this.width / 2) - 7;

	var me = this;
	this.boundsChangedListener =
		google.maps.event.addListener(this.map, "bounds_changed", function() {
			return me.panMap.apply(me);
		});

	// Once the properties of this OverlayView are initialized, set its map so
	// that we can display it.  This will trigger calls to panes_changed and
	// draw.
	this.setMap(this.map);
}

/* InfoBox extends GOverlay class from the Google Maps API
 */
if (typeof google === 'object' && typeof google.maps === 'object') {
	InfoBox.prototype = new google.maps.OverlayView();
}

/* Creates the DIV representing this InfoBox
 */
InfoBox.prototype.remove = function() {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
		this.setMap(null);
	}
};

/* Redraw the Bar based on the current projection and zoom level
 */
InfoBox.prototype.draw = function() {
	// Creates the element if it doesn't exist already.
	this.createElement();
	if (!this.div) return;

	// Calculate the DIV coordinates of two opposite corners of our bounds to
	// get the size and position of our Bar
	var pixPosition = this.getProjection().fromLatLngToDivPixel(this.latlng);
	var o = this.options;
	if (!pixPosition) return;
	// Now position our DIV based on the DIV coordinates of our bounds
	this.div.style.width = this.width + "px";
	this.div.style.left = (pixPosition.x + this.offsetHorizontal) + "px";
	this.div.style.height = this.height + "px";
	this.div.style.top = (pixPosition.y + this.offsetVertical) + "px";
	this.div.style.display = 'block';
};

/* Creates the DIV representing this InfoBox in the floatPane.  If the panes
 * object, retrieved by calling getPanes, is null, remove the element from the
 * DOM.  If the div exists, but its parent is not the floatPane, move the div
 * to the new pane.
 * Called from within draw.  Alternatively, this can be called specifically on
 * a panes_changed event.
 */
InfoBox.prototype.createElement = function() {
	var panes = this.getPanes();
	var that = this;
	var o = this.options;
	var div = this.div;
	if (!div) {
		// This does not handle changing panes.  You can set the map to be null and
		// then reset the map to move the div.
		div = this.div = document.createElement("div");
		div.style.position = "absolute";
		// div.style.background = o.backgroundImage;
		div.style.width = this.width + "px";
		div.style.height = this.height + "px";
		var contentDiv = document.createElement("div");
		contentDiv.innerHTML = o.content;

		var topDiv = document.createElement("div");
		var closeImg = contentDiv.getElementsByClassName("close");

		$(closeImg).on("click", function() {
			that.remove();
		});

		div.appendChild(topDiv);
		div.appendChild(contentDiv);
		div.style.display = 'none';
		panes.floatPane.appendChild(div);
		this.panMap();
	} else if (div.parentNode != panes.floatPane) {
		// The panes have changed.  Move the div.
		div.parentNode.removeChild(div);
		panes.floatPane.appendChild(div);
	} else {
		// The panes have not changed, so no need to create or move the div.
	}
}

/* Pan the map to fit the InfoBox.
 */
InfoBox.prototype.panMap = function() {
	// if we go beyond map, pan map
	var map = this.map;
	var bounds = map.getBounds();
	if (!bounds) return;

	// The position of the infowindow
	var position = this.latlng;

	// The dimension of the infowindow
	var iwWidth = this.width;
	var iwHeight = this.height;
	// The offset position of the infowindow
	var iwOffsetX = this.offsetHorizontal;
	var iwOffsetY = this.offsetVertical;
	// Padding on the infowindow
	var padX = 40;
	var padY = 40;

	// The degrees per pixel
	var mapDiv = map.getDiv();
	var mapWidth = mapDiv.offsetWidth;
	var mapHeight = mapDiv.offsetHeight;
	var boundsSpan = bounds.toSpan();
	var longSpan = boundsSpan.lng();
	var latSpan = boundsSpan.lat();
	var degPixelX = longSpan / mapWidth;
	var degPixelY = latSpan / mapHeight;

	// The bounds of the map
	var mapWestLng = bounds.getSouthWest().lng();
	var mapEastLng = bounds.getNorthEast().lng();
	var mapNorthLat = bounds.getNorthEast().lat();
	var mapSouthLat = bounds.getSouthWest().lat();

	// The bounds of the infowindow
	var iwWestLng = position.lng() + (iwOffsetX - padX) * degPixelX;
	var iwEastLng = position.lng() + (iwOffsetX + iwWidth + padX) * degPixelX;
	var iwNorthLat = position.lat() - (iwOffsetY - padY) * degPixelY;
	var iwSouthLat = position.lat() - (iwOffsetY + iwHeight + padY) * degPixelY;

	// calculate center shift
	var shiftLng =
		(iwWestLng < mapWestLng ? mapWestLng - iwWestLng : 0) +
		(iwEastLng > mapEastLng ? mapEastLng - iwEastLng : 0);
	var shiftLat =
		(iwNorthLat > mapNorthLat ? mapNorthLat - iwNorthLat : 0) +
		(iwSouthLat < mapSouthLat ? mapSouthLat - iwSouthLat : 0);

	// The center of the map
	var center = map.getCenter();

	// The new map center
	var centerX = center.lng() - shiftLng;
	var centerY = center.lat() - shiftLat;

	// center the map to the new shifted center
	map.setCenter(new google.maps.LatLng(centerY, centerX));

	// Remove the listener after panning is complete.
	google.maps.event.removeListener(this.boundsChangedListener);
	this.boundsChangedListener = null;
};
