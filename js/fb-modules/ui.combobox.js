/**
 * @author alexander.farkas
 */
(function($, document) {
	var isRTL = ($('html').attr('dir') === 'rtl');
	var blockSetHooks = false;
	var listItemID = 0;
	var boxSizing = Modernizr.prefixed('boxSizing');
	$.widget('ui.combobox', {
		options: {
			overlay: {
				hideStyle: 'display',
				positionType: 'around',
				hideWindowedFlash: false,
				animShow: function(jElm, ui) {
					jElm.css(ui.posCSS).css({
						display: 'block'
					});
				},
				animHide: function(jElm, ui) { //Hide-Animation 
					jElm.css({
						display: 'none'
					});
				},
				positionOpts: {
					horizontal: 'sameleft',
					vertical: 'bottom',
					fitToView: 'vertical'
				}
			},
			drawDownBtn: true,
			assignSelectWidth: true,
			addTextboxWidthToDatalist: true,
			addMaxHeight: true, //or number
			data: false,
			debug: false,
			hugeTreshold: 10,
			fixedPlaceholderOption: false,
			useTextSpan: false,
			datalistAfterCombobox: false,
			useJScrollPane: false

			/*
			data: {
						selectedIndex: 0,
						defaultSelected: 0,
						label: false,
						items: [
							{name: 'Option 1', val: 'o1'},
							{name: 'Option 2', val: 'o2'},
							{name: 'Option 3', className: }
							]
					}
			 */
		},
		customEvents: ['init', 'create', 'shadowlistcreate', 'select', 'change'],
		_create: function() {
			if (isRTL) {
				return;
			}
			if (this.element.is('datalist > *')) {
				console.log('abort combobxo for datalist > select', this.element);
				return;
			}
			this._getMarkupOptions();

			var o = this.options,
				that = this,
				downBtn = (o.drawDownBtn) ? '<span>&#9662;</span>' : '',
				corners = {
					cb: '',
					list: ''
				},
				className = this.element[0].className || '',
				input = o.useTextSpan ? ' <span role="combobox" class="text-input" tabindex="0" /> ' : ' <input type="text" role="combobox" readonly="readonly" aria-readonly="false" unselectable="on" /> ';



			if (o.datalistAfterCombobox) {
				o.overlay.positionOpts.offsetType = 'position';
			}
			this.isNativeSelect = $.nodeName(this.element[0], 'select');

			if (this.isNativeSelect || !o.data) {

				if (this.isNativeSelect) {
					this.element.addClass('ui-replaced');
				}
				this.createDataFromSelect();
			}

			this.combobox = $('<span class="combobox combobox-closed' + className + '" tabindex="-1"> ' + corners.cb + input + downBtn + '</span>');

			if (className) {
				className = $.map(className.split(' '), function(cName) {
					return 'datalist-' + cName;
				}).join(' ');
			}

			this.dataList = $('<div class="datalist ' + className + '" role="listbox"><div class="datalist-box" role="presentation"><div class="datalist-innerbox" role="presentation">' + corners.list + '</div></div></div>');

			this.textbox = $('input, span.text-input', this.combobox);

			if (o.data.label && o.data.label[0]) {
				this.textbox.labelWith(o.data.label);
				o.data.label.bind('click', function(e) {
					that.combobox.setFocus();
					e.preventDefault();
					return false;
				});
			}

			if (this.isNativeSelect) {

				this.combobox.insertAfter(this.element);
				$.webshims.ready('dom-support', function() {
					$.webshims.addShadowDom(that.element, that.combobox);
				});
				if (!$.webshims.addShadowDom && $.webshims._polyfill) {
					$.webshims.ready('WINDOWLOAD', function() {
						$.webshims._polyfill(['dom-support']);
					});
				}
				if (o.assignSelectWidth) {
					this._assignSelectWidth();
				}

				this.element.hide();
			} else {
				this.combobox.insertAfter(this.element);
			}
			this.beforeShowVal = false;



			this.refresh();
			if (o.fixedPlaceholderOption && o.data && o.data.items) {
				this.setTextBoxVal(o.data.items[0].name);
			}

			if (this.isNativeSelect) {

				o.disabled = this.element.prop('disabled');
				if (this.element[0].form) {
					$(this.element[0].form).bind('reset', function(e) {
						if (e && !e.isDefaultPrevented()) {
							setTimeout(function() {
								that.changeIndex(that.element[0].selectedIndex);
							}, 0);
						}
					});
				}
				this.element.bind('change', function(e) {
					if (e && e.originalEvent) {
						that.changeIndex(this.selectedIndex);
					}
				});
				$(document).userMode(function(e) {
					if (e.enabled) {
						that.showNative();
					} else {
						that.showReplacer();
					}
				});
			}

			this._addFocusinFocusout(this.combobox, this);


			this.combobox
				.bind('mousedown', function(e) {
					that.toggleVisibility(e);
					that.combobox.setFocus({
						fast: true
					});
					return false;
				});
			this.selectedIndex = null;
			if (o.disabled) {
				this._setOption('disabled', true);
			}
			this._trigger('init', {
				type: 'init'
			}, this.ui());
		},
		updateSelectUi: function() {

			if (!this.isOpen && this.element.hasClass('ui-replaced')) {
				this.element.css('display', '');
				this._assignSelectWidth();
				this.element.css('display', 'none');
			}
			if (!this.options.datalistAfterCombobox && this.isOpen) {
				this.dataList.css(
					this.overlayInstance.setPosition({
						type: 'reposition',
						target: this.overlayInstance.currentOpener[0]
					}, {
						opener: this.overlayInstance.currentOpener
					})
				);
			}
		},
		_addFocusinFocusout: function(elem, that) {

			elem
				.bind('focusin focus', function(e) {
					that._createShadowDataList();
					clearTimeout(that.inActiveTimer);
					e.stopImmediatePropagation();
					that.combobox.addClass('combobox-active');
					that.textbox.attr({
						tabindex: '-1'
					});
					that.element.trigger('focusin');

					setTimeout(function() {
						clearTimeout(that.inActiveTimer);
					}, 9);
				})
				.bind('focusout', function(e) {

					e.stopImmediatePropagation();
					clearTimeout(that.inActiveTimer);
					that.inActiveTimer = setTimeout(function() {
						that.textbox.attr({
							tabindex: '0'
						});
						that.combobox.removeClass('combobox-active');
						that.element.trigger('focusout');
						that.close(e);
					}, 9);

				});
		},
		_createShadowDataList: function() {
			if (this._isDataListCreated) {
				return;
			}
			this._isDataListCreated = true;
			var that = this;
			var o = this.options;
			var key = '';
			var charTimer;

			if (o.data.label && o.data.label[0]) {
				this.dataList.labelWith(o.data.label);
			}

			this.refresh();
			if (o.datalistAfterCombobox) {
				this.dataList.insertAfter(this.combobox);
			}
			this.dataList
				.addClass('a11y-js-overflow')

			.bind('coverlaybeforeshow', function(e) {
					that.beforeShowVal = that.value;
					if (o.addTextboxWidthToDatalist) {
						that.dataList.css('width', that.combobox[(o.addTextboxWidthToDatalist === true) ? 'outerWidth' : o.addTextboxWidthToDatalist]());
					}
					if (o.addMaxHeight) {
						that._addMaxHeight();
					}
				})
				.bind('coverlaybeforehide', function(e) {
					if (that.beforeShowVal !== false && that.beforeShowVal !== that.value) {
						that._trigger('change', e, that.ui());

						if (that.isNativeSelect) {
							that.element.trigger('change');
						}
					}
				})
				.bind('coverlayhide', function(e) {
					that.textbox.attr({
						'aria-expanded': 'false'
					});

					setTimeout(function() {
						that.textbox.removeAttr('aria-activedescendant');
					}, 1);

					that.dataList.attr({
						'aria-expanded': 'false'
					});
					that.combobox.removeClass('js-datalist-visible');
					that.selectedIndex = null;
					that.beforeShowVal = false;
					that.items.removeClass('js-selected');
				})
				.bind('coverlayshow', function(e) {
					that.textbox.attr({
						'aria-expanded': 'true'
					});
					that.dataList.attr({
						'aria-expanded': 'true'
					});
					that.combobox.addClass('js-datalist-visible');

					if (o.useJScrollPane) {
						if (typeof $.fn.jScrollPane !== 'function') {
							console.warn('jScrollPane not found, please visit http://jscrollpane.kelvinluck.com/');
							return;
						}
						var jspEl = that.dataList.find('ul[role="presentation"]');
						jspEl.data('jsp') ? jspEl.data('jsp').reinitialise() : jspEl.jScrollPane();

					}
				})
				//prevent close form focusout
				.bind('mousedown', function(e) {
					clearTimeout(that.inActiveTimer);
					setTimeout(function() {
						clearTimeout(that.inActiveTimer);
					}, 1);
				})
				.cOverlay(o.overlay);
			this._addFocusinFocusout(this.dataList, this);

			this.overlayInstance = this.dataList.data('cOverlay') || this.dataList.data('uiCOverlay');


			this.dataList
				.delegate('li:not(.option-group)', 'click', function(e) {
					if (this.getAttribute('aria-disabled') == 'true') {
						return;
					}
					that.select(that.items.index(this), e, true);
					that.combobox.setFocus();
					that.close(e);
				})
				.delegate('li:not(.option-group)', 'mouseenter', function() {
					if (!that.overlayInstance.isVisible || this.getAttribute('aria-disabled') == 'true') {
						return;
					}
					if (that.selectedIndex != null) {
						that.items.eq(that.selectedIndex).removeClass('js-selected');
						that.selectedIndex = null;
					}
					that.selectedIndex = that.items.index(this);
					$(this).addClass('js-selected');
					setTimeout(function() {
						that.textbox.activateThis(that.items.eq(that.selectedIndex));
					}, 1);
				});
			this.hasComboboxLayout = !!((parseInt(this.combobox.css('paddingBottom'), 10) || 0) + (parseInt(this.combobox.css('borderBottomWidth'), 10) || 0));
			if (o.addMaxHeight === true) {
				o.addMaxHeight = parseInt($('div.datalist-innerbox > ul', this.dataList).css('maxHeight'), 10) || true;
			}
			this.combobox
				.bind('keypress', function(e) {
					var foundItem = false,
						code,
						lastKey;
					if (e.charCode) {
						code = e.charCode;
					} else {
						code = e.which;
					}

					if (code && code >= 32) {
						lastKey = String.fromCharCode(code).toUpperCase();
						key += lastKey;

						clearTimeout(charTimer);
						charTimer = setTimeout(function() {
							key = '';
						}, 350);
						foundItem = that.findItemByChar(key);
						if (foundItem === false) {
							foundItem = that.findItemByChar(lastKey);
							key = (foundItem !== false) ? lastKey : '';
						}

					} else {
						key = '';
					}

					if (foundItem !== false) {
						that.select(foundItem, e);
						return false;
					}
				})
				.bind('keydown', function(e) {
					var index = (that.selectedIndex != null) ? that.selectedIndex : that.checkedIndex,
						foundItem = false,
						key;
					if (e.keyCode === $.ui.keyCode.RIGHT || e.keyCode === $.ui.keyCode.DOWN) {
						foundItem = that.getNextOption(index, 1);
					} else if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.UP) {
						foundItem = that.getNextOption(index, -1);
					} else if (e.keyCode === $.ui.keyCode.END) {
						foundItem = that.getNextOption(that.items.length, -1);
					} else if (e.keyCode === $.ui.keyCode.HOME) {
						foundItem = that.getNextOption(-1, 1);
					} else if (e.keyCode === $.ui.keyCode.PAGE_UP) {
						foundItem = that.getNextOption(Math.max(index - 6, -1), 1);
					} else if (e.keyCode === $.ui.keyCode.PAGE_DOWN) {
						foundItem = that.getNextOption(Math.min(index + 6, that.items.length), -1);
					} else if (e.keyCode === $.ui.keyCode.ENTER) {
						if (that.selectedIndex != null) {
							that.select(that.selectedIndex, e);
						}
						that.close(e);
						return false; /* SPECIAL: ENTER: select + close */
					}

					if (foundItem !== false) {
						that.select(foundItem, e);
						return false;
					}
				});
			$(document).bind('mousedown', function(e) {
				if (that.isOpen && !$.contains(that.dataList[0], e.target)) {
					that.close(e);
				}
			});
			this._trigger('shadowlistcreate', {
				type: 'shadowlistcreate'
			}, this.ui());
		},
		findItemByChar: function(key) {
			var foundItem = false;
			var that = this;
			$.each(this.options.data.items, function(i, item) {
				if (i !== that.checkedIndex && !item.disabled && item.name.toUpperCase().indexOf(key) === 0) {
					if (foundItem === false) {
						foundItem = i;
					}
					if (i > that.checkedIndex) {
						foundItem = i;
						return false;
					}
				}
			});
			return foundItem;
		},
		getNextOption: function(pos, dir) {
			var items = this.options.data.items;
			var found = pos;

			while (pos >= -1 && pos <= items.length) {
				pos += dir;
				if (items[pos] && !items[pos].disabled) {
					found = pos;
					break;
				}

			}
			return found;
		},
		showNative: function() {
			this.combobox.hide();
			this.element.removeClass('ui-replaced').show();
		},
		showReplacer: function() {
			this.combobox.show();
			this.element.hide().addClass('ui-replaced');
		},
		_setOption: function(name, value) {
			if (name === 'disabled') {
				value = !!value;
				this.isDisabled = value;
				if (!blockSetHooks && this.isNativeSelect) {
					this.element.prop('disabled', value);
				}
				if (!this.isAborted) {
					this.combobox[value ? 'addClass' : 'removeClass']('disabled');
					this.textbox.attr(
						value ? {
							'aria-disabled': 'true',
							tabindex: '-1'
						} : {
							'aria-disabled': 'false',
							tabindex: '0'
						}
					);
				}
			}
			$.Widget.prototype._setOption.apply(this, arguments);
		},
		ui: function(obj) {
			var ret = {
				instance: this,
				element: this.element,
				combobox: this.combobox,
				dataList: this.dataList,
				items: this.items,
				options: this.options,
				text: this.text,
				value: this.value,
				checkedIndex: this.checkedIndex
			};
			if (obj) {
				$.extend(ret, obj);
			}
			return ret;
		},
		_flatNestedItems: function(flat, nestedItems) {
			var that = this;
			$.each(nestedItems, function(i, item) {
				if (!item.items) {
					flat.push(item);
				} else {
					that._flatNestedItems(flat, item);
				}
			});
		},
		refresh: function(data) {
			if (!this.dataList) {
				return;
			}
			var o = this.options,
				items = '',
				cItem;

			o.data = data || o.data;
			if (this.isNativeSelect && !data) {
				this.createDataFromSelect();
			}

			if (!o.data.items && o.data.nestedItems) {
				o.data.items = [];
				this._flatNestedItems(o.data.items, o.data.nestedItems);
			}

			if (o.data.items.length > o.hugeTreshold) {
				this.dataList.addClass('huge-datalist');
			} else {
				this.dataList.removeClass('huge-datalist');
			}

			if (this._isDataListCreated) {
				if (o.data.items && !o.data.nestedItems) {
					o.data.nestedItems = o.data.items;
				}
				cItem = function(list) {

					$.each(list, function(i, item) {
						var attrs = [];
						listItemID++;
						if (item.items) {
							items += '<li class="option-group" role="presentation"><span class="optgroup-label" id="optgroup-' + listItemID + '">' + item.label + '</span><ul role="listbox" aria-labelledby="optgroup-' + listItemID + '">';
							cItem(item.items);
							items += '</ul></li>';
						} else {
							if (item.className) {
								attrs.push(' class="' + item.className + '"');
							}
							if (item.disabled) {
								attrs.push(' aria-disabled="true"');
							}
							if (item.val === undefined) {
								item.val = item.name;
							}

							items += '<li ' + attrs.join(" ") + 'data-val="' + encodeURIComponent(item.val) + '" role="option" tabindex="-1" id="listoption-' + listItemID + '"><span>' + item.name + '</span></li>';
						}
					});
				};
				cItem(o.data.nestedItems);
				$('div.datalist-innerbox', this.dataList).html('<ul role="presentation" style="position: relative;">' + items + '</ul>');

				this.items = $('li:not(.option-group)', this.dataList);
			}

			this.changeIndex(o.data.selectedIndex || 0);
		},
		update: function() {
			this.changeIndex(this.element.prop('selectedIndex'));
		},
		_assignSelectWidth: function() {
			var addPixel = function(elem, styles) {
				var num = 0;
				$.each(styles, function(i, style) {
					num += parseInt(elem.css(style), 10) || 0;
				});
				return num;
			};
			var outerWidth;

			this.combobox.css({
				marginLeft: this.element.css('marginLeft'),
				marginRight: this.element.css('marginRight'),
				display: 'none',
				width: 'auto'
			});

			outerWidth = this.element.outerWidth();
			this.combobox.css('display', 'inline-block');

			if (boxSizing && this.element.css(boxSizing) == 'border-box') {
				this.textbox.css(boxSizing, 'border-box').css({
					width: outerWidth - addPixel(this.combobox, ['paddingLeft', 'paddingRight', 'borderLeftWidth', ''])
				});
			} else {
				if (boxSizing) {
					this.textbox.css(boxSizing, '');
				}
				this.textbox.outerWidth(outerWidth);
			}

		},
		_iterateChilds: function(element, flatList, nestedList, data) {
			var that = this;
			$('> option, > optgroup', element).each(function(i) {
				var option = $(this);
				var items;
				var itemData;
				if (option.is('optgroup')) {
					items = [];
					nestedList.push({
						label: option.attr('label') || '',
						items: items
					});
					that._iterateChilds(option, flatList, items, data);
				} else {
					if (this.defaultSelected) {
						data.defaultSelected = i;
					}
					itemData = {
						name: $.trim(option.text()),
						val: option.val(),
						className: this.className,
						disabled: $.prop(this, 'disabled')
					};
					flatList.push(itemData);
					nestedList.push(itemData);
				}
			});
		},
		createDataFromSelect: function() {
			var o = this.options;
			o.data = {
				selectedIndex: this.element[0].selectedIndex,
				defaultSelected: 0,
				label: $('label[for=' + this.element.getID() + ']', this.element[0].form || document.body),
				items: [],
				nestedItems: []
			};
			this._iterateChilds(this.element, o.data.items, o.data.nestedItems, o.data);
			o.data.defaultSelected = o.data.defaultSelected || 0;
		},
		toggleVisibility: function(e) {
			this[(!this.overlayInstance || !this.overlayInstance.isVisible) ? 'open' : 'close'](e);
		},
		_addMaxHeight: function() {
			var offset = this.combobox.offset().top - $(window).scrollTop(),
				substr = this.combobox.outerHeight() + 20,
				maxHeight = Math.max($(window).height() - offset, offset) - substr;
			if (this.options.addMaxHeight && this.options.addMaxHeight !== true) {
				maxHeight = Math.min(maxHeight, this.options.addMaxHeight);
			}
			$('div.datalist-innerbox > ul', this.dataList).css({
				maxHeight: maxHeight,
				overflowY: 'auto',
				overflowX: 'hidden'
			});
		},
		open: function(e) {
			if (this.isDisabled) {
				return;
			}
			this.isOpen = true;
			this.combobox.removeClass('combobox-closed');
			this._createShadowDataList();
			var evt = {
				type: e.type,
				target: e.target,
				currentTarget: e.currentTarget,
				pageX: e.pageX,
				pageY: e.pageY,
				clientX: e.clientX,
				clientY: e.clientY
			};

			if (this.options.datalistAfterCombobox) {
				evt.target = this.combobox[0];
				evt.currentTarget = this.combobox[0];
			} else if (!this.hasComboboxLayout) {
				evt.target = this.textbox[0];
				evt.currentTarget = this.textbox[0];
			}
			this.overlayInstance.show(evt, this.ui());
		},
		close: function(e) {
			if (this.options.debug) {
				return;
			}
			this.overlayInstance.hide(e, this.ui());
			this.combobox.addClass('combobox-closed');
			this.isOpen = false;
		},
		setTextBoxVal: function(val) {
			this.textbox[this.options.useTextSpan ? 'text' : 'val'](val);
		},
		val: function(val) {
			if (val !== undefined) {
				var that = this;
				val = encodeURIComponent('' + val);
				this.items.each(function(i) {
					if (this.getAttribute('data-val') == val) {
						that.changeIndex(i);
						return false;
					}
				});
			}
			return this.value;
		},
		changeIndex: function(i) {
			var options = this.options;
			var data = options.data;
			if (!data || !data.items || i < 0) {
				return;
			}
			var selected = data.items[i];
			if (!selected) {
				return;
			}
			this.value = selected.val;
			this.text = selected.name;

			this.combobox[!this.value ? 'addClass' : 'removeClass']('empty-value');
			this.combobox[i == data.defaultSelected ? 'addClass' : 'removeClass']('default-selected');

			if (!options.fixedPlaceholderOption) {
				this.setTextBoxVal(this.text);
			}
			this.textbox.attr({
				'aria-valuetext': this.text
			});
			if (!this.items) {
				return;
			}

			selected = this.items.eq(i);

			if (selected[0] && i > -1) {
				this.beforeActiveItem = this.items.filter('.js-checked').removeClass('js-checked');
				this.activeItem = selected.addClass('js-checked');
				this.checkedIndex = i;


				if (this.isNativeSelect) {
					if (!blockSetHooks) {
						this.element.val(this.value);
					}
					this.activeOption = this.element.find('option').eq(this.checkedIndex);
				}


			}
		},
		scrollIntoView: function(elem, e) {

			var list = $('ul', this.dataList);
			var scrollTop = list.scrollTop();
			var oldPos = scrollTop;
			var pos = elem.position().top;
			var listHeight;
			var elemHeight;
			var dif;
			if (pos < -1) {
				scrollTop = scrollTop + pos - 2;
			} else {
				listHeight = list.innerHeight();
				elemHeight = elem.outerHeight();
				if (listHeight - 2 < pos + elemHeight) {
					scrollTop = (e && e.type == 'keypress') ?
						scrollTop + pos - 2 :
						scrollTop + pos + elemHeight - listHeight + 2;
				}
			}
			if (oldPos != scrollTop) {
				dif = Math.abs(oldPos - scrollTop);
				list.stop();
				if (dif > 150) {
					list.animate({
						scrollTop: scrollTop
					}, {
						duration: Math.max(Math.min(500, dif * 0.8), 150)
					});
				} else {
					list.scrollTop(scrollTop);
				}
			}
		},
		select: function(i, e, noFocus) {
			e = e || {
				type: 'select'
			};
			$.extend(e, {
				selectboxData: this.ui({
					nextIndex: i
				})
			});
			var that = this;
			var item;
			if (!this.overlayInstance.isVisible) {
				this.open(e);
			}

			this.changeIndex(i);

			if (!noFocus) {
				item = that.items.eq(that.checkedIndex);
				setTimeout(function() {
					that.textbox.activateThis(item);
				}, 1);
				this.scrollIntoView(item, e);
			}
			if (that.selectedIndex != null) {
				that.items.eq(that.selectedIndex).removeClass('js-selected');
				this.selectedIndex = null;
			}

			this._trigger('select', e, this.ui());
		}
	});

	$.webshims.ready('WINDOWLOAD', function() {
		setTimeout(function() {
			var combos;
			var getSelectForUpdate = function() {
				var combo = $.data(this, 'uiCombobox');
				if (combo && combo.updateSelectUi && combo.combobox[0].offsetWidth) {
					combos.push(combo);
				}
			};
			$(document).bind('updateshadowdom', function() {
				combos = [];
				$('select.ui-replaced').each(getSelectForUpdate);
				$('html').addClass('ui-select-hide');
				for (var i = 0; i < combos.length; i++) {
					combos[i].updateSelectUi();
				}
				$('html').removeClass('ui-select-hide');
			});
		}, 9);

	});


	//propHooks
	$.each(['selectedIndex', 'value'], function(i, name) {

		//be hook friendly
		if (!$.propHooks[name]) {
			$.propHooks[name] = {};
		}
		var oldSetHook = $.propHooks[name].set;
		$.propHooks[name].set = function(elem, value) {

			var ret = oldSetHook && oldSetHook(elem, value);
			if (value !== undefined) {
				setTimeout(function() {
					var widget = $.data(elem, 'uiCombobox');
					if (widget) {
						blockSetHooks = true;
						widget.update();
						blockSetHooks = false;
					}
					widget = null;
					elem = null;
				}, 9);
			}

			return ret;
		};
	});
	(function() {
		if (!$.propHooks.disabled) {
			$.propHooks.disabled = {};
		}
		var oldSetHook = $.propHooks.disabled.set;
		$.propHooks.disabled.set = function(elem, value) {
			var ret = oldSetHook && oldSetHook(elem, value);
			if (value !== undefined) {
				var widget = $.data(elem, 'uiCombobox');
				if (widget) {
					blockSetHooks = true;
					widget._setOption('disabled', value);
					blockSetHooks = false;
				}
				widget = null;
			}
			elem = null;
			return ret;
		};
	})();
	(function() {
		if (!$.propHooks.selected) {
			$.propHooks.selected = {};
		}
		var oldSetHook = $.propHooks.selected.set;
		$.propHooks.selected.set = function(elem, value) {
			var ret = oldSetHook && oldSetHook(elem, value);
			var parent;
			if (value !== undefined && $.nodeName(elem, 'option') && (parent = elem.parentNode)) {
				setTimeout(function() {
					var widget = $.data(parent, 'uiCombobox');
					if (widget) {
						blockSetHooks = true;
						widget.update();
						blockSetHooks = false;
					}
					widget = null;
					parent = null;
				}, 9);
			}
			elem = null;
			return ret;
		};
	})();

	(function() {
		$.webshims.addReady(function(context, contextElem) {
			if (context == document) {
				return;
			}
			contextElem
				.filter('option, optgroup, select')
				.closest('select')
				.each(function(i) {
					var combobox = $.data(this, 'uiCombobox');
					if (combobox && combobox.refresh) {
						combobox.refresh();
					}
				});
		});
	})();
})(window.webshims && webshims.$ || jQuery, document);
